#!/bin/sh
#
# docker-entrypoint for service

set -e
echo "Executing java ${JAVA_ARGS} "$@""
java ${JAVA_ARGS} -Xms512m -Xmx2048m -jar eros-retriever-service-0.0.1-SNAPSHOT.jar

