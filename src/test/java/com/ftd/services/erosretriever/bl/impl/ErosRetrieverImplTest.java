package com.ftd.services.erosretriever.bl.impl;

import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.api.response.ServiceResponse;
import com.ftd.services.erosretriever.bl.ErosRetriever;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

public class ErosRetrieverImplTest extends BaseApplicationTest {

    @Mock
    private ErosRetriever erosRetriever;

    private ServiceResponse serviceResponse;

    private static final String SPACE = "";

    @Before
    public void setUp() {
        serviceResponse = new ServiceResponse();
    }

    @Test
    public void testGetNewMessageList() {
        ServiceResponse expected = serviceResponse;
        when(erosRetriever.getNewMessageList(anyString(), anyString())).thenReturn(serviceResponse);
        assertEquals(expected, erosRetriever.getNewMessageList(SPACE, SPACE));
    }

    @Test
    public void testGetMessageDetails() {
        ServiceResponse expected = serviceResponse;
        when(erosRetriever.getMessageDetails(Mockito.anyString(), Mockito.anyLong(), Mockito.anyString())).thenReturn(serviceResponse);
        assertEquals(expected, erosRetriever.getMessageDetails(SPACE, 1L, SPACE));
    }
}
