package com.ftd.services.erosretriever.bl.impl;

import com.ftd.eapi.message.service.v1.CodeMessagePair;
import com.ftd.eapi.message.service.v1.MessageDetailResponse;
import com.ftd.eapi.message.service.v1.MultiMessageNewListResponse;
import com.ftd.eapi.message.service.v1.NewMessage;
import com.ftd.eapi.message.service.v1.NewMessageList;
import com.ftd.eapi.message.service.v1.NewMessageListResponse;
import com.ftd.eapi.message.service.v1.ObjectFactory;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.SingleMessageResponse;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.api.response.ServiceResponse;
import com.ftd.services.erosretriever.util.ErosSoapClient;
import com.ftd.services.erosretriever.util.InboundMessageHandler;
import com.ftd.services.erosretriever.util.MercuryEapiUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import javax.xml.bind.JAXBElement;

import static com.ftd.services.erosretriever.config.GlobalConstants.SUCCESS;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class ErosRetrieverImplIT extends BaseApplicationTest {

    private ErosRetrieverImpl erosRetriever = null;

    @Mock
    private ErosSoapClient erosSoapClient;

    @Mock
    private MercuryEapiUtil mercuryEapiUtil;

    @Mock
    private InboundMessageHandler inboundMessageHandler;

    private JAXBElement<NewMessageListResponse> noNewMessageListResponseJAXBElement = null;

    private JAXBElement<NewMessageListResponse> newMessageListResponseJAXBElement = null;

    private JAXBElement<MessageDetailResponse> messageDetailResponseJAXBElement = null;

    @Before
    public void setUp() throws Exception {
        //Given
        erosRetriever = new ErosRetrieverImpl();
        erosRetriever.setMercuryEapiUtil(mercuryEapiUtil);
        erosRetriever.setErosSoapClient(erosSoapClient);
        erosRetriever.setInboundMessageHandler(inboundMessageHandler);

        ObjectFactory factory = new ObjectFactory();
        NewMessageListResponse newMessageListResponse = new NewMessageListResponse();
        newMessageListResponse.setMultiMessageNewListResponse(new MultiMessageNewListResponse());
        noNewMessageListResponseJAXBElement = factory.createNewMessageListResponse(newMessageListResponse);

        NewMessageListResponse newMessageListResponse1 = new NewMessageListResponse();
        MultiMessageNewListResponse multiMessageNewListResponse = new MultiMessageNewListResponse();
        NewMessageList newMessageList = new NewMessageList();
        newMessageList.getNewMessage().add(new NewMessage());
        multiMessageNewListResponse.setNewMessageList(newMessageList);
        multiMessageNewListResponse.setHasMoreMessages(false);

        newMessageListResponse1.setMultiMessageNewListResponse(multiMessageNewListResponse);
        newMessageListResponseJAXBElement = factory.createNewMessageListResponse(newMessageListResponse1);
    }

    @Test
    public void testWhenNoMessagesInTheList() {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(noNewMessageListResponseJAXBElement);

        //When
        ServiceResponse serviceResponse = erosRetriever.getNewMessageList(floristId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertNotNull(serviceResponse.getMessage());
        assertEquals("no new messages", serviceResponse.getMessage());
        assertNull(serviceResponse.getType());
    }

    @Test
    public void testGetMessageDetailsForFTDOrder() throws Exception {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        Long messageId = 1234L;

        ObjectFactory factory = new ObjectFactory();
        MessageDetailResponse messageDetailResponse = new MessageDetailResponse();
        SingleMessageResponse singleMessageResponse = new SingleMessageResponse();
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        processedErosMessage.setErosMessage(getSampleErosOrderMessage());
        singleMessageResponse.setProcessedErosMessage(processedErosMessage);
        CodeMessagePair codeMessagePair = new CodeMessagePair();
        codeMessagePair.setCode("VERIFIED");
        singleMessageResponse.setResponseCode(codeMessagePair);
        messageDetailResponse.setSingleMessageResponse(singleMessageResponse);
        messageDetailResponseJAXBElement = factory.createMessageDetailResponse(messageDetailResponse);

        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(messageDetailResponseJAXBElement);
        doNothing().when(inboundMessageHandler).ftdOrder(messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage(),
                                                         messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage().getErosMessage(),
                                                         siteId);

        //When
        ServiceResponse serviceResponse = erosRetriever.getMessageDetails(floristId, messageId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals(SUCCESS, serviceResponse.getType());
    }

    @Test
    public void testGetMessageDetailsForAskMessage() throws Exception {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        Long messageId = 1234L;

        ObjectFactory factory = new ObjectFactory();
        MessageDetailResponse messageDetailResponse = new MessageDetailResponse();
        SingleMessageResponse singleMessageResponse = new SingleMessageResponse();
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        processedErosMessage.setErosMessage(getSampleAskMessage());
        singleMessageResponse.setProcessedErosMessage(processedErosMessage);
        CodeMessagePair codeMessagePair = new CodeMessagePair();
        codeMessagePair.setCode("VERIFIED");
        singleMessageResponse.setResponseCode(codeMessagePair);
        messageDetailResponse.setSingleMessageResponse(singleMessageResponse);
        messageDetailResponseJAXBElement = factory.createMessageDetailResponse(messageDetailResponse);

        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(messageDetailResponseJAXBElement);
        doNothing().when(inboundMessageHandler).askMessage(messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage(),
                messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage().getErosMessage(),
                siteId);

        //When
        ServiceResponse serviceResponse = erosRetriever.getMessageDetails(floristId, messageId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals(SUCCESS, serviceResponse.getType());
    }

    @Test
    public void testGetMessageDetailsForAnswerMessage() throws Exception {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        Long messageId = 1234L;

        ObjectFactory factory = new ObjectFactory();
        MessageDetailResponse messageDetailResponse = new MessageDetailResponse();
        SingleMessageResponse singleMessageResponse = new SingleMessageResponse();
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        processedErosMessage.setErosMessage(getSampleAnswerMessage());
        singleMessageResponse.setProcessedErosMessage(processedErosMessage);
        CodeMessagePair codeMessagePair = new CodeMessagePair();
        codeMessagePair.setCode("VERIFIED");
        singleMessageResponse.setResponseCode(codeMessagePair);
        messageDetailResponse.setSingleMessageResponse(singleMessageResponse);
        messageDetailResponseJAXBElement = factory.createMessageDetailResponse(messageDetailResponse);

        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(messageDetailResponseJAXBElement);
        doNothing().when(inboundMessageHandler).ansMessage(messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage(),
                messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage().getErosMessage(),
                siteId);

        //When
        ServiceResponse serviceResponse = erosRetriever.getMessageDetails(floristId, messageId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals(SUCCESS, serviceResponse.getType());
    }

    @Test
    public void testGetMessageDetailsForCancelOrderMessage() throws Exception {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        Long messageId = 1234L;

        ObjectFactory factory = new ObjectFactory();
        MessageDetailResponse messageDetailResponse = new MessageDetailResponse();
        SingleMessageResponse singleMessageResponse = new SingleMessageResponse();
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        processedErosMessage.setErosMessage(getSampleCancelOrderMessage());
        singleMessageResponse.setProcessedErosMessage(processedErosMessage);
        CodeMessagePair codeMessagePair = new CodeMessagePair();
        codeMessagePair.setCode("VERIFIED");
        singleMessageResponse.setResponseCode(codeMessagePair);
        messageDetailResponse.setSingleMessageResponse(singleMessageResponse);
        messageDetailResponseJAXBElement = factory.createMessageDetailResponse(messageDetailResponse);

        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(messageDetailResponseJAXBElement);
        doNothing().when(inboundMessageHandler).cancelOrder(messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage(),
                messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage().getErosMessage(),
                siteId);

        //When
        ServiceResponse serviceResponse = erosRetriever.getMessageDetails(floristId, messageId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals(SUCCESS, serviceResponse.getType());
    }

    @Test
    public void testGetMessageDetailsForConfirmCancelMessage() throws Exception {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        Long messageId = 1234L;

        ObjectFactory factory = new ObjectFactory();
        MessageDetailResponse messageDetailResponse = new MessageDetailResponse();
        SingleMessageResponse singleMessageResponse = new SingleMessageResponse();
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        processedErosMessage.setErosMessage(getSampleConfirmCancelMessage());
        singleMessageResponse.setProcessedErosMessage(processedErosMessage);
        CodeMessagePair codeMessagePair = new CodeMessagePair();
        codeMessagePair.setCode("VERIFIED");
        singleMessageResponse.setResponseCode(codeMessagePair);
        messageDetailResponse.setSingleMessageResponse(singleMessageResponse);
        messageDetailResponseJAXBElement = factory.createMessageDetailResponse(messageDetailResponse);

        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(messageDetailResponseJAXBElement);
        doNothing().when(inboundMessageHandler).confirmCancel(messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage(),
                messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage().getErosMessage(),
                siteId);

        //When
        ServiceResponse serviceResponse = erosRetriever.getMessageDetails(floristId, messageId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals(SUCCESS, serviceResponse.getType());
    }

    @Test
    public void testGetMessageDetailsForDenyCancelMessage() throws Exception {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        Long messageId = 1234L;

        ObjectFactory factory = new ObjectFactory();
        MessageDetailResponse messageDetailResponse = new MessageDetailResponse();
        SingleMessageResponse singleMessageResponse = new SingleMessageResponse();
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        processedErosMessage.setErosMessage(getSampleDenyCancelMessage());
        singleMessageResponse.setProcessedErosMessage(processedErosMessage);
        CodeMessagePair codeMessagePair = new CodeMessagePair();
        codeMessagePair.setCode("VERIFIED");
        singleMessageResponse.setResponseCode(codeMessagePair);
        messageDetailResponse.setSingleMessageResponse(singleMessageResponse);
        messageDetailResponseJAXBElement = factory.createMessageDetailResponse(messageDetailResponse);

        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(messageDetailResponseJAXBElement);
        doNothing().when(inboundMessageHandler).denyCancel(messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage(),
                messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage().getErosMessage(),
                siteId);

        //When
        ServiceResponse serviceResponse = erosRetriever.getMessageDetails(floristId, messageId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals(SUCCESS, serviceResponse.getType());
    }

    @Test
    public void testGetMessageDetailsForGeneralMessage() throws Exception {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        Long messageId = 1234L;

        ObjectFactory factory = new ObjectFactory();
        MessageDetailResponse messageDetailResponse = new MessageDetailResponse();
        SingleMessageResponse singleMessageResponse = new SingleMessageResponse();
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        processedErosMessage.setErosMessage(getSampleGeneralMessage());
        singleMessageResponse.setProcessedErosMessage(processedErosMessage);
        CodeMessagePair codeMessagePair = new CodeMessagePair();
        codeMessagePair.setCode("VERIFIED");
        singleMessageResponse.setResponseCode(codeMessagePair);
        messageDetailResponse.setSingleMessageResponse(singleMessageResponse);
        messageDetailResponseJAXBElement = factory.createMessageDetailResponse(messageDetailResponse);

        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(messageDetailResponseJAXBElement);
        doNothing().when(inboundMessageHandler).generalMessage(messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage(),
                messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage().getErosMessage(),
                siteId);

        //When
        ServiceResponse serviceResponse = erosRetriever.getMessageDetails(floristId, messageId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals(SUCCESS, serviceResponse.getType());
    }

    @Test
    public void testGetMessageDetailsForRejectMessage() throws Exception {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        Long messageId = 1234L;

        ObjectFactory factory = new ObjectFactory();
        MessageDetailResponse messageDetailResponse = new MessageDetailResponse();
        SingleMessageResponse singleMessageResponse = new SingleMessageResponse();
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        processedErosMessage.setErosMessage(getSampleRejectMessage());
        singleMessageResponse.setProcessedErosMessage(processedErosMessage);
        CodeMessagePair codeMessagePair = new CodeMessagePair();
        codeMessagePair.setCode("VERIFIED");
        singleMessageResponse.setResponseCode(codeMessagePair);
        messageDetailResponse.setSingleMessageResponse(singleMessageResponse);
        messageDetailResponseJAXBElement = factory.createMessageDetailResponse(messageDetailResponse);

        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(messageDetailResponseJAXBElement);
        doNothing().when(inboundMessageHandler).rejectOrder(messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage(),
                messageDetailResponseJAXBElement.getValue().getSingleMessageResponse().getProcessedErosMessage().getErosMessage(),
                siteId);

        //When
        ServiceResponse serviceResponse = erosRetriever.getMessageDetails(floristId, messageId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals(SUCCESS, serviceResponse.getType());
    }

    @Test
    public void testWhenMessagesInTheList() {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(newMessageListResponseJAXBElement);
        when(mercuryEapiUtil.enqueueMessageId(anyString(), anyLong(), anyObject())).thenReturn(true);
        when(mercuryEapiUtil.confirmMessage(anyString(), anyString())).thenReturn(true);

        //When
        ServiceResponse serviceResponse = erosRetriever.getNewMessageList(floristId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals("Processed Successfully", serviceResponse.getMessage());
        assertNull(serviceResponse.getType());
    }

    @Test
    public void testWhenMessagesInTheListThrowsException() {
        //Given
        String floristId = "04-1391AA";
        String siteId = "ftd";
        when(erosSoapClient.call(anyObject(), anyString())).thenReturn(newMessageListResponseJAXBElement);

        //When
        ServiceResponse serviceResponse = erosRetriever.getNewMessageList(floristId, siteId);

        //Then
        assertNotNull(serviceResponse);
        assertEquals("Processed Successfully", serviceResponse.getMessage());
        assertNull(serviceResponse.getType());
    }
}
