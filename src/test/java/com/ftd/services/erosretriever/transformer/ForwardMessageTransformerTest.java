package com.ftd.services.erosretriever.transformer;

import com.ftd.eapi.message.service.v1.ForwardMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.impl.ForwardMessageTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class ForwardMessageTransformerTest extends BaseApplicationTest {
    @Mock
    private ForwardMessageTransformer forwardMessageTransformer;

    private FloristMessageNotification floristMessageNotification;

    @Before
    public void setUp() {
        floristMessageNotification = new FloristMessageNotification();
    }

    @Test
    public void testTransform() {
        FloristMessageNotification expected = floristMessageNotification;
        Mockito.when(forwardMessageTransformer.transform(any(ProcessedErosMessage.class), any(ForwardMessage.class))).thenReturn(floristMessageNotification);
        assertEquals(expected, forwardMessageTransformer.transform(new ProcessedErosMessage(), new ForwardMessage()));
    }
}
