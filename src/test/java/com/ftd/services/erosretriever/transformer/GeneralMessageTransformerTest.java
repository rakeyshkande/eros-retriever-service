package com.ftd.services.erosretriever.transformer;

import com.ftd.eapi.message.service.v1.GeneralMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.impl.GeneralMessageTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class GeneralMessageTransformerTest extends BaseApplicationTest {
    @Mock
    private GeneralMessageTransformer generalMessageTransformer;

    private FloristMessageNotification floristMessageNotification;

    @Before
    public void setUp() {

        floristMessageNotification = new FloristMessageNotification();
    }

    @Test
    public void testTransform() {
        FloristMessageNotification expected = floristMessageNotification;
        Mockito.when(generalMessageTransformer.transform(any(ProcessedErosMessage.class), any(GeneralMessage.class))).thenReturn(floristMessageNotification);
        assertEquals(expected, generalMessageTransformer.transform(new ProcessedErosMessage(), new GeneralMessage()));
    }
}
