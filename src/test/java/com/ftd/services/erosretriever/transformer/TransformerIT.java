package com.ftd.services.erosretriever.transformer;

import com.ftd.eapi.message.service.v1.AnswerMessage;
import com.ftd.eapi.message.service.v1.AskMessage;
import com.ftd.eapi.message.service.v1.CancelOrderMessage;
import com.ftd.eapi.message.service.v1.ConfirmCancelMessage;
import com.ftd.eapi.message.service.v1.DenyCancelMessage;
import com.ftd.eapi.message.service.v1.ForwardMessage;
import com.ftd.eapi.message.service.v1.GeneralMessage;
import com.ftd.eapi.message.service.v1.OrderRelatedMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.RejectOrderMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.service.OrderService;
import com.ftd.services.erosretriever.transformer.impl.AnswerMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.AskMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.CancelOrderMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.ConfirmCancelMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.DenyCancelMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.ForwardMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.GeneralMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.RejectOrderMessageTransformer;
import com.ftd.services.erosretriever.types.EventType;
import com.ftd.services.erosretriever.util.InboundMessageHandler;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import static com.ftd.services.erosretriever.util.DateUtils.toDate;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;

public class TransformerIT extends BaseApplicationTest {

    @Autowired
    private CancelOrderMessageTransformer cancelOrderMessageTransformer;

    @Autowired
    private AskMessageTransformer askMessageTransformer;

    @Autowired
    private AnswerMessageTransformer answerMessageTransformer;

    @Autowired
    private RejectOrderMessageTransformer rejectOrderMessageTransformer;

    @Autowired
    private GeneralMessageTransformer generalMessageTransformer;

    @Autowired
    private ConfirmCancelMessageTransformer confirmCancelMessageTransformer;

    @Autowired
    private DenyCancelMessageTransformer denyCancelMessageTransformer;

    @Autowired
    private ForwardMessageTransformer forwardMessageTransformer;

    private String siteId = null;

    @Autowired
    private InboundMessageHandler inboundMessageHandler;

    @Mock
    private OrderService orderService;

    @Before
    public void setUp() {
        siteId = "FTD";

    }

    @Test
    public void testAskTransformer() throws Exception {
        //Given
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        AskMessage askMessage = getSampleAskMessage();
        processedErosMessage.setErosMessage(askMessage);

        //When
        FloristMessageNotification floristMessageNotification = askMessageTransformer.transform(processedErosMessage, askMessage);

        //Then
        assertEquals(floristMessageNotification.getData().getPrice(), askMessage.getPrice());
        assertErosOrderNotification(floristMessageNotification, processedErosMessage, askMessage, EventType.FULFILLMENT_COMMUNICATION);
    }

    @Test
    public void testForwardTransformer() throws Exception {
        //Given
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        ForwardMessage forwardMessage = getSampleForwardMessage();
        processedErosMessage.setErosMessage(forwardMessage);

        //When
        FloristMessageNotification floristMessageNotification = forwardMessageTransformer.transform(processedErosMessage, forwardMessage);

        //Then
        assertEquals(floristMessageNotification.getData().getNewFillerMemberCode(), forwardMessage.getNewFillerMemberCode());
        assertErosOrderNotification(floristMessageNotification, processedErosMessage, forwardMessage, EventType.FULFILLMENT_COMMUNICATION);
    }

    @Test
    public void testAnswerTransformer() throws Exception {
        //Given
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        AnswerMessage answerMessage = getSampleAnswerMessage();
        processedErosMessage.setErosMessage(answerMessage);

        //When
        FloristMessageNotification floristMessageNotification = answerMessageTransformer.transform(processedErosMessage, answerMessage);

        //Then
        assertEquals(floristMessageNotification.getData().getPrice(), answerMessage.getPrice());
        assertEquals(floristMessageNotification.getEventName(), EventType.FULFILLMENT_COMMUNICATION);
        assertEquals(floristMessageNotification.getFulfillmentChannel(), GlobalConstants.FULFILLMENT_CHANNEL);
        assertEquals(floristMessageNotification.getSource(), GlobalConstants.SOURCE);
        assertErosOrderNotification(floristMessageNotification, processedErosMessage, answerMessage, EventType.FULFILLMENT_COMMUNICATION);
    }

    @Test
    public void testRejectOrderMessageTransformer() throws Exception {
        //Given
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        RejectOrderMessage rejectOrderMessage = getSampleRejectMessage();
        processedErosMessage.setErosMessage(rejectOrderMessage);

        //When
        FloristMessageNotification floristMessageNotification = rejectOrderMessageTransformer.transform(processedErosMessage, rejectOrderMessage);

        //Then
        assertErosOrderNotification(floristMessageNotification, processedErosMessage, rejectOrderMessage, EventType.REJECT_ORDER);
    }

    @Test
    public void testCancelOrderMessageTransformer() throws Exception {
        //Given
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        CancelOrderMessage cancelOrderMessage = getSampleCancelOrderMessage();
        processedErosMessage.setErosMessage(cancelOrderMessage);

        //When
        FloristMessageNotification floristMessageNotification = cancelOrderMessageTransformer.transform(processedErosMessage, cancelOrderMessage);

        //Then
        assertErosOrderNotification(floristMessageNotification, processedErosMessage, cancelOrderMessage, EventType.FULFILLMENT_COMMUNICATION);
    }

    @Test
    public void testConfirmCancelMessageTransformer() throws Exception {
        //Given
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        ConfirmCancelMessage confirmCancelMessage = getSampleConfirmCancelMessage();
        processedErosMessage.setErosMessage(confirmCancelMessage);

        //When
        FloristMessageNotification floristMessageNotification = confirmCancelMessageTransformer.transform(processedErosMessage, confirmCancelMessage);

        //Then
        assertErosOrderNotification(floristMessageNotification, processedErosMessage, confirmCancelMessage, EventType.FULFILLMENT_COMMUNICATION);
    }

    @Test
    public void testDenyCancelMessageTransformer() throws Exception {
        //Given
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        DenyCancelMessage denyCancelMessage = getSampleDenyCancelMessage();
        processedErosMessage.setErosMessage(denyCancelMessage);

        //When
        FloristMessageNotification floristMessageNotification = denyCancelMessageTransformer.transform(processedErosMessage, denyCancelMessage);

        //Then
        assertErosOrderNotification(floristMessageNotification, processedErosMessage, denyCancelMessage, EventType.FULFILLMENT_COMMUNICATION);
    }

    @Test
    public void testGeneralMessageTransformer() throws Exception {
        //Given
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        GeneralMessage generalMessage = getSampleGeneralMessage();
        processedErosMessage.setErosMessage(generalMessage);

        //When
        FloristMessageNotification floristMessageNotification = generalMessageTransformer.transform(processedErosMessage, generalMessage);

        //Then
        assertEquals(generalMessage.getExternalReferenceNumber(), floristMessageNotification.getData().getExternalReferenceNumber());
        assertEquals(processedErosMessage.getMessageStatus(), floristMessageNotification.getData().getMessageStatus());
        assertEquals(processedErosMessage.getMessageType(), floristMessageNotification.getData().getMessageType());
        assertEquals(processedErosMessage.getMessageId(), floristMessageNotification.getData().getMessageId());
        assertEquals(toDate(processedErosMessage.getMessageReceivedDate()), floristMessageNotification.getData().getMessageReceivedDate());
        assertEquals(generalMessage.getReceivingMemberCode(), floristMessageNotification.getData().getReceivingMemberCode());
        assertEquals(generalMessage.getCorrelationReference(), floristMessageNotification.getData().getCorrelationReference());
        assertEquals(processedErosMessage.getTransmissionId(), floristMessageNotification.getData().getTransmissionId());
        assertEquals(toDate(processedErosMessage.getTransmissionDate()), floristMessageNotification.getData().getTransmissionDate());
        assertEquals(processedErosMessage.getTransmissionStatus(), floristMessageNotification.getData().getTransmissionStatus());
        assertEquals(generalMessage.getOperator(), floristMessageNotification.getData().getOperator());
        assertEquals(processedErosMessage.getAdminSequenceNumber(), floristMessageNotification.getData().getAdminSequenceNumber());
        assertEquals(generalMessage.getService().value(), floristMessageNotification.getData().getService());
        assertEquals(generalMessage.getMessageText(), floristMessageNotification.getData().getMessageText());
    }

    @Test
    public void testCancelOrder() throws Exception {
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        CancelOrderMessage cancelOrderMessage = getSampleCancelOrderMessage();
        processedErosMessage.setErosMessage(cancelOrderMessage);
        FloristMessageNotification floristMessageNotification = cancelOrderMessageTransformer.transform(processedErosMessage, cancelOrderMessage);
        doNothing().when(orderService).orderNotification(floristMessageNotification);
        inboundMessageHandler.cancelOrder(processedErosMessage, cancelOrderMessage, siteId);
    }

    @Test
    public void testAskMessage() throws Exception {
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        AskMessage askMessage = getSampleAskMessage();
        processedErosMessage.setErosMessage(askMessage);
        FloristMessageNotification floristMessageNotification = askMessageTransformer.transform(processedErosMessage, askMessage);
        doNothing().when(orderService).orderNotification(floristMessageNotification);
        inboundMessageHandler.askMessage(processedErosMessage, askMessage, siteId);
    }

    @Test
    public void testAnsMessage() throws Exception {
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        AnswerMessage answerMessage = getSampleAnswerMessage();
        processedErosMessage.setErosMessage(answerMessage);
        FloristMessageNotification floristMessageNotification = answerMessageTransformer.transform(processedErosMessage, answerMessage);
        doNothing().when(orderService).orderNotification(floristMessageNotification);
        inboundMessageHandler.ansMessage(processedErosMessage, answerMessage, siteId);
    }

    @Test
    public void testRejectOrder() throws Exception {
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        RejectOrderMessage rejectOrderMessage = getSampleRejectMessage();
        processedErosMessage.setErosMessage(rejectOrderMessage);
        FloristMessageNotification floristMessageNotification = rejectOrderMessageTransformer.transform(processedErosMessage, rejectOrderMessage);
        doNothing().when(orderService).orderNotification(floristMessageNotification);
        inboundMessageHandler.rejectOrder(processedErosMessage, rejectOrderMessage, siteId);
    }

    @Test
    public void testGeneralMessage() throws Exception {
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        GeneralMessage generalMessage = getSampleGeneralMessage();
        processedErosMessage.setErosMessage(generalMessage);
        FloristMessageNotification floristMessageNotification = generalMessageTransformer.transform(processedErosMessage, generalMessage);
        doNothing().when(orderService).orderNotification(floristMessageNotification);
        inboundMessageHandler.generalMessage(processedErosMessage, generalMessage, siteId);
    }

    @Test
    public void testConfirmCancel() throws Exception {
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        ConfirmCancelMessage confirmCancelMessage = getSampleConfirmCancelMessage();
        processedErosMessage.setErosMessage(confirmCancelMessage);
        FloristMessageNotification floristMessageNotification = confirmCancelMessageTransformer.transform(processedErosMessage, confirmCancelMessage);
        doNothing().when(orderService).orderNotification(floristMessageNotification);
        inboundMessageHandler.confirmCancel(processedErosMessage, confirmCancelMessage, siteId);
    }

    @Test
    public void testDenyCancel() throws Exception {
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        DenyCancelMessage denyCancelMessage = getSampleDenyCancelMessage();
        processedErosMessage.setErosMessage(denyCancelMessage);
        FloristMessageNotification floristMessageNotification = denyCancelMessageTransformer.transform(processedErosMessage, denyCancelMessage);
        doNothing().when(orderService).orderNotification(floristMessageNotification);
        inboundMessageHandler.denyCancel(processedErosMessage, denyCancelMessage, siteId);
    }

    private void assertErosOrderNotification(FloristMessageNotification floristMessageNotification,
                                             ProcessedErosMessage processedErosMessage,
                                             OrderRelatedMessage orderRelatedMessage,
                                             EventType eventType) {
        assertEquals(orderRelatedMessage.getRecipientAddress(), floristMessageNotification.getData().getRecipient().getAddress1());
        assertEquals(orderRelatedMessage.getReasonText(), floristMessageNotification.getData().getReasonText());
        assertEquals(toDate(orderRelatedMessage.getDeliveryDate()), floristMessageNotification.getData().getDeliveryDate());
        assertEquals(orderRelatedMessage.getExternalReferenceNumber(), floristMessageNotification.getData().getExternalReferenceNumber());
        assertEquals(processedErosMessage.getMessageStatus(), floristMessageNotification.getData().getMessageStatus());
        assertEquals(processedErosMessage.getMessageType(), floristMessageNotification.getData().getMessageType());
        assertEquals(processedErosMessage.getMessageId(), floristMessageNotification.getData().getMessageId());
        assertEquals(toDate(processedErosMessage.getMessageReceivedDate()), floristMessageNotification.getData().getMessageReceivedDate());
        assertEquals(orderRelatedMessage.getReceivingMemberCode(), floristMessageNotification.getData().getReceivingMemberCode());
        assertEquals(orderRelatedMessage.getCorrelationReference(), floristMessageNotification.getData().getCorrelationReference());
        assertEquals(processedErosMessage.getTransmissionId(), floristMessageNotification.getData().getTransmissionId());
        assertEquals(toDate(processedErosMessage.getTransmissionDate()), floristMessageNotification.getData().getTransmissionDate());
        assertEquals(processedErosMessage.getTransmissionStatus(), floristMessageNotification.getData().getTransmissionStatus());
        assertEquals(orderRelatedMessage.getOperator(), floristMessageNotification.getData().getOperator());
        assertEquals(processedErosMessage.getAdminSequenceNumber(), floristMessageNotification.getData().getAdminSequenceNumber());
        assertEquals(orderRelatedMessage.getOrderDateString(), floristMessageNotification.getData().getOrderDate());
        assertEquals(orderRelatedMessage.getService().value(), floristMessageNotification.getData().getService());
        assertEquals(floristMessageNotification.getEventName(), eventType);
        assertEquals(floristMessageNotification.getFulfillmentChannel(), GlobalConstants.FULFILLMENT_CHANNEL);
        assertEquals(floristMessageNotification.getSource(), GlobalConstants.SOURCE);
    }
}
