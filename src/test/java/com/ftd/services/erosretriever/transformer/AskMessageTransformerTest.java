package com.ftd.services.erosretriever.transformer;

import com.ftd.eapi.message.service.v1.AskMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.impl.AskMessageTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class AskMessageTransformerTest extends BaseApplicationTest {
    @Mock
    private AskMessageTransformer askMessageTransformer;

    private FloristMessageNotification floristMessageNotification;

    @Before
    public void setUp() {
        floristMessageNotification = new FloristMessageNotification();
    }

    @Test
    public void testTransform() {
        FloristMessageNotification expected = floristMessageNotification;
        Mockito.when(askMessageTransformer.transform(any(ProcessedErosMessage.class), any(AskMessage.class))).thenReturn(floristMessageNotification);
        assertEquals(expected, askMessageTransformer.transform(new ProcessedErosMessage(), new AskMessage()));
    }
}
