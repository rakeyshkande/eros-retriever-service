package com.ftd.services.erosretriever.transformer;

import com.ftd.eapi.message.service.v1.DenyCancelMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.impl.DenyCancelMessageTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class DenyCancelMessageTransformerTest extends BaseApplicationTest {
    @Mock
    private DenyCancelMessageTransformer denyCancelMessageTransformer;

    private FloristMessageNotification floristMessageNotification;

    @Before
    public void setUp() {
        floristMessageNotification = new FloristMessageNotification();
    }
    @Test
    public void testTransform() {
        FloristMessageNotification expected = floristMessageNotification;
        Mockito.when(denyCancelMessageTransformer.transform(any(ProcessedErosMessage.class), any(DenyCancelMessage.class))).thenReturn(floristMessageNotification);
        assertEquals(expected, denyCancelMessageTransformer.transform(new ProcessedErosMessage(), new DenyCancelMessage()));
    }
}
