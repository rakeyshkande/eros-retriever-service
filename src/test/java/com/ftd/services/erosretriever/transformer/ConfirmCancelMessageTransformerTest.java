package com.ftd.services.erosretriever.transformer;

import com.ftd.eapi.message.service.v1.ConfirmCancelMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.impl.ConfirmCancelMessageTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class ConfirmCancelMessageTransformerTest extends BaseApplicationTest {
    @Mock
    private ConfirmCancelMessageTransformer confirmCancelMessageTransformer;

    private FloristMessageNotification floristMessageNotification;

    @Before
    public void setUp() {
        floristMessageNotification = new FloristMessageNotification();
    }
    @Test
    public void testTransform() {
        FloristMessageNotification expected = floristMessageNotification;
        Mockito.when(confirmCancelMessageTransformer.transform(any(ProcessedErosMessage.class), any(ConfirmCancelMessage.class))).thenReturn(floristMessageNotification);
        assertEquals(expected, confirmCancelMessageTransformer.transform(new ProcessedErosMessage(), new ConfirmCancelMessage()));
    }
}
