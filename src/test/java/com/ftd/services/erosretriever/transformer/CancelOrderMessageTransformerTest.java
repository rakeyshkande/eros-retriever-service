package com.ftd.services.erosretriever.transformer;

import com.ftd.eapi.message.service.v1.CancelOrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.impl.CancelOrderMessageTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class CancelOrderMessageTransformerTest extends BaseApplicationTest {
    @Mock
    private CancelOrderMessageTransformer cancelOrderMessageTransformer;

    private FloristMessageNotification floristMessageNotification;

    @Before
    public void setUp() {
        floristMessageNotification = new FloristMessageNotification();
    }

    @Test
    public void testTransform() {
        FloristMessageNotification expected = floristMessageNotification;
        Mockito.when(cancelOrderMessageTransformer.transform(any(ProcessedErosMessage.class), any(CancelOrderMessage.class))).thenReturn(expected);
        assertEquals(expected, cancelOrderMessageTransformer.transform(new ProcessedErosMessage(), new CancelOrderMessage()));
    }
}
