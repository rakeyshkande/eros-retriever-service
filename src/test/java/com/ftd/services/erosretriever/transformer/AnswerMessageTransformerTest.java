package com.ftd.services.erosretriever.transformer;

import com.ftd.eapi.message.service.v1.AnswerMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.impl.AnswerMessageTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class AnswerMessageTransformerTest extends BaseApplicationTest {

    @Mock
    private AnswerMessageTransformer answerMessageTransformer;

    private FloristMessageNotification floristMessageNotification;

    @Before
    public void setUp() {
        floristMessageNotification = new FloristMessageNotification();
    }

    @Test
    public void testTransform() {
        FloristMessageNotification expected = floristMessageNotification;
        Mockito.when(answerMessageTransformer.transform(any(ProcessedErosMessage.class), any(AnswerMessage.class))).thenReturn(floristMessageNotification);
        assertEquals(expected, answerMessageTransformer.transform(new ProcessedErosMessage(), new AnswerMessage()));
    }
}
