package com.ftd.services.erosretriever.transformer;

import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.RejectOrderMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.impl.RejectOrderMessageTransformer;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class RejectOrderMessageTransformerTest extends BaseApplicationTest {
    @Mock
    private RejectOrderMessageTransformer rejectOrderMessageTransformer;

    private FloristMessageNotification floristMessageNotification;

    @Before
    public void setUp() {

        floristMessageNotification = new FloristMessageNotification();
    }
    @Test
    public void testTransform() {
        FloristMessageNotification expected = floristMessageNotification;
        Mockito.when(rejectOrderMessageTransformer.transform(any(ProcessedErosMessage.class), any(RejectOrderMessage.class))).thenReturn(floristMessageNotification);
        assertEquals(expected, rejectOrderMessageTransformer.transform(new ProcessedErosMessage(), new RejectOrderMessage()));
    }
}
