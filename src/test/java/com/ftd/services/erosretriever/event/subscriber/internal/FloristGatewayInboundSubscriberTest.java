package com.ftd.services.erosretriever.event.subscriber.internal;

import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.api.response.ServiceResponse;
import com.ftd.services.erosretriever.bl.ErosRetriever;
import com.ftd.services.erosretriever.domain.event.FloristInboundEvent;
import com.google.gson.Gson;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

public class FloristGatewayInboundSubscriberTest extends BaseApplicationTest {

    @Mock
    private ErosRetriever erosRetriever;

    @Autowired
    private FloristGatewayInboundSubscriber floristGatewayInboundSubscriber;

    @Test
    public final void testOnSubscribe() throws JSONException {
        //Given
        JSONObject json = new JSONObject();
        json.put("floristId", "40-1234AA");
        json.put("messageId", "123456");
        json.put("siteId", "ftd");
        String messageString = json.toString();
        PubsubMessage messageMock = PubsubMessage.newBuilder().setData(ByteString.copyFrom(messageString.getBytes()))
                .build();

        Gson gson = new Gson();
        FloristInboundEvent floristInboundEvent = gson.fromJson(messageMock.getData().toStringUtf8(), FloristInboundEvent.class);

        BDDMockito.given(erosRetriever.getMessageDetails(floristInboundEvent.getFloristId(), floristInboundEvent.getMessageId(), floristInboundEvent.getSiteId().toString()))
                .willReturn(new ServiceResponse());

        //When
        floristGatewayInboundSubscriber.onSubscribe(messageMock);
    }

}
