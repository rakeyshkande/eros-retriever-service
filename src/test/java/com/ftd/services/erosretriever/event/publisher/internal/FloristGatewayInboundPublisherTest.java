package com.ftd.services.erosretriever.event.publisher.internal;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.config.PubSubConfig;
import com.ftd.services.erosretriever.domain.event.FloristInboundEvent;
import com.ftd.services.erosretriever.domain.event.SiteId;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;

public class FloristGatewayInboundPublisherTest extends BaseApplicationTest {

    @Mock
    private PublisherUtil publisherUtil;

    @Mock
    private PubSubConfig pubSubConfig;

    @InjectMocks
    private FloristGatewayInboundPublisher floristGatewayInboundPublisher;

    private FloristInboundEvent floristInboundEvent;

    private PubSubConfig.PubSub pubSub;

    @Before
    public void init() {
        //Given
        pubSub = new PubSubConfig.PubSub();
        pubSub.setFloristGatewayInboundTopic("testFloristGatewayInboundTopic");

        floristInboundEvent = new FloristInboundEvent();
        floristInboundEvent.setFloristId("04-1391AA");
        floristInboundEvent.setMessageId(22753897);
        floristInboundEvent.setSiteId(SiteId.ftd);
    }

    @Test
    public final void testPublish() {
        Gson gson = new Gson();
        String msgDataMock = gson.toJson(floristInboundEvent);
        BDDMockito.given(pubSubConfig.getPubSub()).willReturn(pubSub);
        BDDMockito.given(publisherUtil.publish(msgDataMock, pubSub.getFloristGatewayInboundTopic())).willReturn(true);

        //When
        floristGatewayInboundPublisher.publish(floristInboundEvent);
    }

}
