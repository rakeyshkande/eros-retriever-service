package com.ftd.services.erosretriever.event.publisher;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.api.request.OGSRequest;
import com.ftd.services.erosretriever.config.PubSubConfig;
import com.ftd.services.erosretriever.dto.OGSOrder;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

public class OrderGatewayOutboundPublisherTest extends BaseApplicationTest {

    @Mock
    private PublisherUtil publisherUtil;

    @Mock
    private PubSubConfig pubSubConfig;

    @Autowired
    private OrderGatewayOutboundPublisher orderGatewayOutboundPublisher;

    private OGSRequest ogsRequest;

    private PubSubConfig.PubSub pubSub;

    @Before
    public void init() {
        //Given
        pubSub = new PubSubConfig.PubSub();
        pubSub.setOrderGatewayOutboundTopic("testOrderGatewayOutboundTopic");

        OGSOrder ogsOrder = new OGSOrder();
        ogsOrder.setOrderId("22753897");
        ogsOrder.setOrderChannel("Webgifts");

        ogsRequest = new OGSRequest();
        ogsRequest.setOrder(ogsOrder);
    }

    @Test
    public final void testPublish() {
        Gson gson = new Gson();
        String message = gson.toJson(ogsRequest);
        BDDMockito.given(pubSubConfig.getPubSub()).willReturn(pubSub);
        BDDMockito.given(publisherUtil.publish(message, pubSub.getOrderGatewayOutboundTopic())).willReturn(true);

        //When
        orderGatewayOutboundPublisher.publish(ogsRequest);
    }
}
