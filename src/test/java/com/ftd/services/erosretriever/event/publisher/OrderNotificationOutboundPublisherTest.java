package com.ftd.services.erosretriever.event.publisher;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.config.PubSubConfig;
import com.ftd.services.erosretriever.domain.Data;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

public class OrderNotificationOutboundPublisherTest extends BaseApplicationTest {
    @Mock
    private PublisherUtil publisherUtil;

    @Mock
    private PubSubConfig pubSubConfig;

    @Autowired
    private OrderNotificationOutboundPublisher orderNotificationOutboundPublisher;

    private FloristMessageNotification floristMessageNotification;

    private PubSubConfig.PubSub pubSub;

    @Before
    public void init() {
        //Given
        pubSub = new PubSubConfig.PubSub();
        pubSub.setOrderNotificationOutboundTopic("testOrderNotificationOutboundTopic(");

        floristMessageNotification = new FloristMessageNotification();
        floristMessageNotification.setData(Data.builder().build());
        floristMessageNotification.getData().setSendingMemberCode("40-13111AA");
        floristMessageNotification.getData().setSendingMemberCode("90-13111AA");
    }

    @Test
    public final void testPublish() {
        Gson gson = new Gson();
        String message = gson.toJson(floristMessageNotification);
        BDDMockito.given(pubSubConfig.getPubSub()).willReturn(pubSub);
        BDDMockito.given(publisherUtil.publish(message, pubSub.getOrderNotificationOutboundTopic())).willReturn(true);

        //When
        orderNotificationOutboundPublisher.publish(floristMessageNotification);
    }
}
