package com.ftd.services.erosretriever.event.publisher;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.config.PubSubConfig;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class FailoverOutboundPublisherTest {

    @Mock
    private PublisherUtil publisherUtil;

    @Mock
    private PubSubConfig pubSubConfig;

    @InjectMocks
    private FailoverOutboundPublisher failoverOutboundPublisher = new FailoverOutboundPublisher();

    private ProcessedErosMessage processedErosMessage;

    private PubSubConfig.PubSub pubSub;

    @Before
    public void init() {
        //Given
        pubSub = new PubSubConfig.PubSub();
        pubSub.setFloristGatewayInboundTopic("testFloristGatewayInboundTopic");

        processedErosMessage = new ProcessedErosMessage();
        processedErosMessage.setMessageId(22753897L);
        processedErosMessage.setMessageType("ASK Message");
    }

    @Test
    public final void testPublish() {
        Gson gson = new Gson();
        String msgDataMock = gson.toJson(processedErosMessage);
        BDDMockito.given(pubSubConfig.getPubSub()).willReturn(pubSub);
        BDDMockito.given(publisherUtil.publish(msgDataMock, pubSub.getFloristGatewayInboundTopic())).willReturn(true);

        //When
        failoverOutboundPublisher.publish(processedErosMessage, pubSub.getFloristGatewayInboundTopic());
    }
}
