package com.ftd.services.erosretriever;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

public class ErosRetrieverApplicationTest extends BaseApplicationTest {

    @Mock
    private ErosRetrieverApplication erosRetrieverApplication;

    @Test
    public void testRestTemplate() {
        //Given
        RestTemplate restTemplate = new RestTemplate();
        RestTemplate expected = restTemplate;
        when(erosRetrieverApplication.restTemplate()).thenReturn(restTemplate);

        //When and Then
        Assert.assertEquals(expected, erosRetrieverApplication.restTemplate());
    }

    @Test
    public void testJackson2JsonObjectMapper() {
        //Given
        Jackson2JsonObjectMapper jackson2JsonObjectMapper = new Jackson2JsonObjectMapper();
        when(erosRetrieverApplication.jackson2JsonObjectMapper(anyObject())).thenReturn(jackson2JsonObjectMapper);

        //When and Then
        Assert.assertEquals(jackson2JsonObjectMapper, erosRetrieverApplication.jackson2JsonObjectMapper(new ObjectMapper()));
    }
}
