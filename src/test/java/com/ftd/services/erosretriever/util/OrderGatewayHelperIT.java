package com.ftd.services.erosretriever.util;

import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.dto.ApplicableCharges;
import com.ftd.services.erosretriever.dto.DeliveryGroups;
import com.ftd.services.erosretriever.dto.DiscountedCharges;
import com.ftd.services.erosretriever.dto.Fees;
import com.ftd.services.erosretriever.dto.LineItems;
import com.ftd.services.erosretriever.dto.NameValue;
import com.ftd.services.erosretriever.dto.OGSOrder;
import com.ftd.services.erosretriever.dto.PaymentMethod;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.ftd.services.erosretriever.config.GlobalConstants.OGS_CURRENCY_CODE;
import static com.ftd.services.erosretriever.config.GlobalConstants.OGS_PAYMENT_TYPE_NO_PAYMENT;
import static com.ftd.services.erosretriever.config.GlobalConstants.WEBGIFTS_DELIVERY_TYPE;
import static com.ftd.services.erosretriever.config.GlobalConstants.WEBGIFTS_FULFILLMENT_CHANNEL;
import static com.ftd.services.erosretriever.config.GlobalConstants.WEBGIFTS_PRODUCT_TYPE;
import static com.ftd.services.erosretriever.config.GlobalConstants.WEBGIFTS_SERVICE_LEVEL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class OrderGatewayHelperIT extends BaseApplicationTest {

    @Autowired
    private OrderGatewayHelper orderGatewayHelper;

    @Test
    public void testBuildOrder() throws Exception {
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        OrderMessage orderMessage = getSampleErosOrderMessage();
        processedErosMessage.setErosMessage(orderMessage);
        OGSOrder ogsOrder = orderGatewayHelper.buildOrder(orderMessage, processedErosMessage);
        assertEquals(processedErosMessage.getNotification(), ogsOrder.getOrderId());
        assertEquals(processedErosMessage.getSender().getAddress(), ogsOrder.getBuyerDetails().getAddress1());
        assertEquals(processedErosMessage.getSender().getCity(), ogsOrder.getBuyerDetails().getCity());
        assertEquals(processedErosMessage.getSender().getPostalCode(), ogsOrder.getBuyerDetails().getPostalCode());
        assertEquals(processedErosMessage.getSender().getStateCode(), ogsOrder.getBuyerDetails().getState());
        for (DeliveryGroups deliveryGroup : ogsOrder.getDeliveryGroups()) {
            assertEquals(orderMessage.getCardMessage(), deliveryGroup.getGiftMessage());
            assertEquals(orderMessage.getOccasionCode().value(), deliveryGroup.getOccasion());
            assertEquals(orderMessage.getRecipientCountryCode(), deliveryGroup.getRecipientDetails().getCountryCode());
            assertEquals(orderMessage.getRecipientStreetAddress(), deliveryGroup.getRecipientDetails().getAddress1());
            assertEquals(orderMessage.getRecipientCity(), deliveryGroup.getRecipientDetails().getCity());
            assertEquals(orderMessage.getRecipientPostalCode(), deliveryGroup.getRecipientDetails().getPostalCode());
        }
        assertEquals(OGS_CURRENCY_CODE, ogsOrder.getPaymentDetails().getCurrencyCode());
        for (PaymentMethod paymentMethod : ogsOrder.getPaymentDetails().getPaymentMethod()) {
            assertEquals(OGS_PAYMENT_TYPE_NO_PAYMENT, paymentMethod.getPaymentMethodType());
        }
        for (DeliveryGroups deliveryGroups : ogsOrder.getDeliveryGroups()) {
            for (LineItems lineItems : deliveryGroups.getLineItems()) {
                assertEquals(processedErosMessage.getNotification(), lineItems.getLineItemId());
                assertEquals(orderMessage.getFirstChoiceProductCode(), lineItems.getProductId());
                assertEquals(orderMessage.getFirstChoiceProductCode(), lineItems.getWebsiteId());
                assertEquals(WEBGIFTS_FULFILLMENT_CHANNEL, lineItems.getFulfillmentChannel());
                assertEquals(WEBGIFTS_SERVICE_LEVEL, lineItems.getServiceLevel());
                assertEquals(WEBGIFTS_PRODUCT_TYPE, lineItems.getProductType());
                assertEquals(WEBGIFTS_DELIVERY_TYPE, lineItems.getDeliveryType());
                assertNull(lineItems.getAccessories());

                Fees fees = lineItems.getFees();
                ApplicableCharges applicableCharges = fees.getApplicableCharges();
                assertNotNull(applicableCharges);
                assertTrue(applicableCharges.getDeliveryCharges().isEmpty());
                DiscountedCharges discountedCharges = fees.getDiscountedCharges();
                assertNotNull(discountedCharges);
                assertTrue(discountedCharges.getDeliveryCharges().isEmpty());
                assertTrue(discountedCharges.getSurCharges().isEmpty());

                for (NameValue nameValue : lineItems.getLineItemAmounts()) {
                    assertNotNull(nameValue.getName());
                    assertNotNull(nameValue.getValue());
                }
            }
        }
    }
}
