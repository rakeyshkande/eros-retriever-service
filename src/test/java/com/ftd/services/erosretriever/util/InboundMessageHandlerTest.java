package com.ftd.services.erosretriever.util;

import com.ftd.eapi.message.service.v1.ErosMessage;
import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.service.OrderGatewayService;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;

public class InboundMessageHandlerTest extends BaseApplicationTest {

    @Mock
    private InboundMessageHandler mockInboundMessageHandler;

    @Autowired
    private InboundMessageHandler inboundMessageHandler;

    @Mock
    private OrderGatewayService orderGatewayService;

    private String siteId = "FTD";

    @Test
    public void testMockFtdOrder() {
        Mockito.doNothing().when(mockInboundMessageHandler).ftdOrder(isA(ProcessedErosMessage.class), isA(ErosMessage.class), isA(String.class));
    }

    @Test
    public void testMockCancelOrder() {
        Mockito.doNothing().when(mockInboundMessageHandler).cancelOrder(isA(ProcessedErosMessage.class), isA(ErosMessage.class), isA(String.class));
    }

    @Test
    public void testMockAskMessage() {
        Mockito.doNothing().when(mockInboundMessageHandler).askMessage(isA(ProcessedErosMessage.class), isA(ErosMessage.class), isA(String.class));
    }

    @Test
    public void testMockAnsMessage() {
        Mockito.doNothing().when(mockInboundMessageHandler).ansMessage(isA(ProcessedErosMessage.class), isA(ErosMessage.class), isA(String.class));
    }

    @Test
    public void testMockRejectOrder() {
        Mockito.doNothing().when(mockInboundMessageHandler).rejectOrder(isA(ProcessedErosMessage.class), isA(ErosMessage.class), isA(String.class));
    }

    @Test
    public void testMockGeneralMessage() {
        Mockito.doNothing().when(mockInboundMessageHandler).generalMessage(isA(ProcessedErosMessage.class), isA(ErosMessage.class), isA(String.class));
    }

    @Test
    public void testMockConfirmCancel() {
        Mockito.doNothing().when(mockInboundMessageHandler).confirmCancel(isA(ProcessedErosMessage.class), isA(ErosMessage.class), isA(String.class));
    }

    @Test
    public void testMockDenyCancel() {
        Mockito.doNothing().when(mockInboundMessageHandler).denyCancel(isA(ProcessedErosMessage.class), isA(ErosMessage.class), isA(String.class));
    }

    @Test
    public void testFtdOrder() throws Exception {
        ProcessedErosMessage processedErosMessage = getSampleProcessedErosMessage();
        OrderMessage orderMessage = getSampleErosOrderMessage();
        doNothing().when(orderGatewayService).processWebgiftOrder(orderMessage, processedErosMessage, siteId);
        inboundMessageHandler.ftdOrder(processedErosMessage, orderMessage, siteId);
    }
}
