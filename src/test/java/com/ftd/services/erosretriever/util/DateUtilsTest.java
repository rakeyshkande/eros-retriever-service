package com.ftd.services.erosretriever.util;

import com.ftd.services.erosretriever.BaseApplicationTest;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DateUtilsTest extends BaseApplicationTest {

    private DateUtils dateUtils = new DateUtils();

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");

    private static String dateString = "10/11/2018";

    private static XMLGregorianCalendar calendar;

    private static Date date;

    private static Date dateTime;

    @BeforeClass
    public static void getTestDate() throws DatatypeConfigurationException, ParseException {
        //Given
        calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(2018, 10, 11, 8, 32, 15, 000, 0);
        date = simpleDateFormat.parse(dateString);
        dateTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").parse("2018-10-11T08:32:15.000+0000");
    }

    @Test
    public void testToDate() {
        Date expected = date;
        //When
        Date actual = dateUtils.toDate(calendar);

        //Then
        assertEquals(expected, actual);
    }

    @Test
    public void testToDateTime() {
        Date expected = dateTime;
        //When
        Date actual = dateUtils.toDateTime(calendar);

        //Then
        assertEquals(expected, actual);
    }

    @Test
    public void testGivenDateIsNullThenReturnsNull() {
        //Given
        Date expected = null;
        //When
        Date actual = dateUtils.toDate(null);

        //Then
        assertEquals(expected, actual);
    }

    @Test
    public void testGivenToDateTimeIsNullThenReturnsNull() {
        //Given
        Date expected = null;
        //When
        Date actual = dateUtils.toDateTime(null);

        //Then
        assertEquals(expected, actual);
    }

}
