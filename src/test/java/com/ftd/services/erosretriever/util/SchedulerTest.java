package com.ftd.services.erosretriever.util;

import com.ftd.services.erosretriever.BaseApplicationTest;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class SchedulerTest extends BaseApplicationTest {

    @Mock
    private Scheduler scheduler;

    @Test
    public void testScheduleTaskWithFixedDelay() {
        Mockito.doNothing().when(scheduler).scheduleTaskWithFixedDelay();
    }

}
