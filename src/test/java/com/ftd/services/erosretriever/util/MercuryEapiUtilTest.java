package com.ftd.services.erosretriever.util;

import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.event.FloristInboundEvent;
import com.ftd.services.erosretriever.domain.event.SiteId;
import com.ftd.services.erosretriever.event.publisher.internal.FloristGatewayInboundPublisher;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;

public class MercuryEapiUtilTest extends BaseApplicationTest {

    @Mock
    private MercuryEapiUtil mockMercuryEapiUtil;

    @Autowired
    private MercuryEapiUtil mercuryEapiUtil;

    @Mock
    private FloristGatewayInboundPublisher floristGatewayInboundPublisher;

    private final Boolean actual = true;

    private final String space = "";

    @Test
    public void testMockConfirmMessage() {
        Mockito.when(mockMercuryEapiUtil.confirmMessage(anyString(), anyString())).thenReturn(actual);
        assertEquals(true, mockMercuryEapiUtil.confirmMessage(space, space));
    }

    @Test
    public void testMockEnqueueMessageId() {
        Mockito.when(mockMercuryEapiUtil.enqueueMessageId(anyString(), anyLong(), anyString())).thenReturn(actual);
        assertEquals(true, mockMercuryEapiUtil.enqueueMessageId(space, 1L, space));
    }

    @Test
    public void testEnqueueMessageId() {
        //Given
        FloristInboundEvent floristInboundEvent = FloristInboundEvent.builder()
                .floristId("04-1391AA")
                .messageId(1234L)
                .siteId(SiteId.valueOf("ftd"))
                .build();
        doNothing().when(floristGatewayInboundPublisher).publish(anyObject());

        //When and Then
        assertEquals(true, mercuryEapiUtil.enqueueMessageId(floristInboundEvent.getFloristId(),
                                                                     floristInboundEvent.getMessageId(),
                                                                     floristInboundEvent.getSiteId().name()));
    }
}
