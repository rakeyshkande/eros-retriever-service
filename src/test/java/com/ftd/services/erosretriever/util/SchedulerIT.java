package com.ftd.services.erosretriever.util;

import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.api.response.ServiceResponse;
import com.ftd.services.erosretriever.bl.ErosRetriever;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.Mockito.when;

public class SchedulerIT extends BaseApplicationTest {

    @Autowired
    private Scheduler scheduler;

    @Mock
    private ErosRetriever erosRetriever;

    @Test
    public void testScheduleTaskWithFixedDelay() {
        when(erosRetriever.getNewMessageList("04-1391AA", "ftd")).thenReturn(new ServiceResponse());
        scheduler.scheduleTaskWithFixedDelay();
    }
}

