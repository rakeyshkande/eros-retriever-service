package com.ftd.services.erosretriever.util;

import com.ftd.eapi.message.service.v1.AskMessage;
import com.ftd.eapi.message.service.v1.ErosMember;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.Service;
import com.ftd.services.erosretriever.BaseApplicationTest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class TransformerUtilTest extends BaseApplicationTest {

    private final ErosMember erosMember = new ErosMember();
    private final ProcessedErosMessage processedErosMessage = new ProcessedErosMessage();
    private final AskMessage askMessage = new AskMessage();

    @Before
    public void setup() {
        erosMember.setAddress("29 Autumn ln");
        erosMember.setBusinessName("FTD");
        erosMember.setCity("Downers Grove");
        erosMember.setMemberCode("04-13211AA");
        erosMember.setPhone("123456789");

        processedErosMessage.setMessageType("FTD Order");
        processedErosMessage.setMessageId(123445L);
        processedErosMessage.setMessageStatus("VERIFIED");

        askMessage.setService(Service.FTD);
        processedErosMessage.setErosMessage(askMessage);
    }

    @Test
    public void testPopulateMember() {
        assertNotNull(TransformerUtil.populateMember(erosMember));
    }

    @Test
    public void testPopulateRecipient() {

        assertNotNull(TransformerUtil.populateRecipient(processedErosMessage));
    }

    @Test
    public void testPopulateOrderRelatedMessageInfo() {
        assertNotNull(TransformerUtil.populateOrderRelatedMessageInfo(processedErosMessage, askMessage));
    }
}
