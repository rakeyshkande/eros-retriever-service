package com.ftd.services.erosretriever.util;

import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.dto.OGSOrder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;

public class OrderGatewayHelperTest extends BaseApplicationTest {

    @Mock
    private OrderGatewayHelper orderGatewayHelper;

    private OGSOrder ogsOrder;

    @Before
    public void setUp() {
        ogsOrder = new OGSOrder();
    }

    @Test
    public void testBuildOrder() {
        OGSOrder expected = ogsOrder;
        Mockito.when(orderGatewayHelper.buildOrder(any(OrderMessage.class), any(ProcessedErosMessage.class))).thenReturn(ogsOrder);
        assertEquals(expected, orderGatewayHelper.buildOrder(new OrderMessage(), new ProcessedErosMessage()));
    }

    @Test
    public void testGetFormattedDate() {
        String actual = "2018-10-10";
        String expected = actual;
        Mockito.when(orderGatewayHelper.getFormattedDate(any(Date.class))).thenReturn(actual);
        assertEquals(expected, orderGatewayHelper.getFormattedDate(new Date()));
    }
}
