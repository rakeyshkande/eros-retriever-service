package com.ftd.services.erosretriever.util;

import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.config.FloristProperties;
import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.WebServiceTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

public class ErosSoapClientTest extends BaseApplicationTest {

    @Mock
    private ErosSoapClient erosSoapClient;

    @Autowired
    private WebServiceTemplate webServiceTemplate;

    @Autowired
    private FloristProperties floristProperties;

    private String space = "";

    private Object object = null;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        erosSoapClient.setWebServiceTemplate(webServiceTemplate);
        erosSoapClient.setFloristProperties(floristProperties);
        object = new Object();
    }

    @Test
    public void testCall() {
        Object expected = object;
        BDDMockito.when(erosSoapClient.call(anyObject(), anyString())).thenReturn(object);
        assertEquals(expected, erosSoapClient.call(object, space));
    }
}
