package com.ftd.services.erosretriever.interceptors;

import com.ftd.services.erosretriever.BaseApplicationTest;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.ws.context.MessageContext;

import static org.mockito.Matchers.isA;

public class WebServiceLogInterceptorTest extends BaseApplicationTest {
    @Mock
    private WebServiceLogInterceptor webServiceLogInterceptor;

    @Test
    public void testHandleRequest() {
        Mockito.when(webServiceLogInterceptor.handleRequest(isA(MessageContext.class))).thenReturn(true);
    }

    @Test
    public void testHandleFault() {
        Mockito.when(webServiceLogInterceptor.handleFault(isA(MessageContext.class))).thenReturn(true);
    }

    @Test
    public void testHandleResponse() {
        Mockito.when(webServiceLogInterceptor.handleResponse(isA(MessageContext.class))).thenReturn(true);
    }

    @Test
    public void testAfterCompletion() {
        Mockito.doNothing().when(webServiceLogInterceptor).afterCompletion(isA(MessageContext.class), isA(Exception.class));
    }
}
