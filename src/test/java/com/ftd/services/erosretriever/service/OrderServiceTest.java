package com.ftd.services.erosretriever.service;

import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Matchers.isA;

public class OrderServiceTest extends BaseApplicationTest {
    @Mock
    private OrderService orderService;

    @Test
    public void testOrderNotification() {
        Mockito.doNothing().when(orderService).orderNotification(isA(FloristMessageNotification.class));
    }
}
