package com.ftd.services.erosretriever.service;

import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.BaseApplicationTest;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Matchers.isA;

public class OrderGatewayServiceTest extends BaseApplicationTest {
    @Mock
    private OrderGatewayService orderGatewayService;

    @Test
    public void testProcessWebgiftOrder() {
        Mockito.doNothing().when(orderGatewayService).processWebgiftOrder(isA(OrderMessage.class), isA(ProcessedErosMessage.class), isA(String.class));
    }
}
