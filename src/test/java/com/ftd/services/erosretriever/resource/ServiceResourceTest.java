package com.ftd.services.erosretriever.resource;

import com.ftd.services.erosretriever.BaseApplicationTest;
import com.ftd.services.erosretriever.api.response.ServiceResponse;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;

public class ServiceResourceTest extends BaseApplicationTest {
    @Mock
    private ServiceResource serviceResource;

    private ServiceResponse serviceResponse;

    private static final String SPACE = "";

    @Before
    public void setUp() {
        serviceResponse = new ServiceResponse();
    }

    @Test
    public void testGetNewMessageList() {
        ServiceResponse expected = serviceResponse;
        Mockito.when(serviceResource.processFloristMessages(anyString(), anyString())).thenReturn(serviceResponse);
        assertEquals(expected, serviceResource.processFloristMessages(SPACE, SPACE));
    }

    @Test
    public void testProcessFloristMessages() {
        Mockito.doNothing().when(serviceResource).processFloristMessages();
        serviceResource.processFloristMessages();
    }

    @Test
    public void testGetMessageDetails() {
        ServiceResponse expected = serviceResponse;
        Mockito.when(serviceResource.getMessageDetails(anyString(), anyLong(), anyString())).thenReturn(serviceResponse);
        assertEquals(expected, serviceResource.getMessageDetails(SPACE, 1L, SPACE));
    }
}
