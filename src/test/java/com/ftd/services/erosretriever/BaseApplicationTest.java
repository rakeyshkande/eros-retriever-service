package com.ftd.services.erosretriever;

import com.ftd.eapi.message.service.v1.AnswerMessage;
import com.ftd.eapi.message.service.v1.AskMessage;
import com.ftd.eapi.message.service.v1.CancelOrderMessage;
import com.ftd.eapi.message.service.v1.ConfirmCancelMessage;
import com.ftd.eapi.message.service.v1.DenyCancelMessage;
import com.ftd.eapi.message.service.v1.ErosMember;
import com.ftd.eapi.message.service.v1.ForwardMessage;
import com.ftd.eapi.message.service.v1.GeneralMessage;
import com.ftd.eapi.message.service.v1.OccasionCode;
import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.RejectOrderMessage;
import com.ftd.eapi.message.service.v1.Service;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.GregorianCalendar;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public abstract class BaseApplicationTest {
    public ProcessedErosMessage getSampleProcessedErosMessage() throws Exception {
        ProcessedErosMessage processedErosMessage = new ProcessedErosMessage();
        processedErosMessage.setMessageId(22844575L);
        processedErosMessage.setMessageStatus("RECEIVED");
        processedErosMessage.setMessageType("FTD Order");
        processedErosMessage.setAdminSequenceNumber(0);
        processedErosMessage.setMessageReceivedDate(getXMLGregorianCalendarNow());
        processedErosMessage.setReferenceNumber("J8637N");
        processedErosMessage.setOrderSequenceNumber(3454);
        processedErosMessage.setTransmissionDate(getXMLGregorianCalendarNow());
        processedErosMessage.setTransmissionId(36003913L);
        processedErosMessage.setTransmissionStatus("RECEIVED");
        processedErosMessage.setSender(getSampleSenderInfo());
        processedErosMessage.setReceiver(getSampleReceiverInfo());

        return processedErosMessage;
    }

    public OrderMessage getSampleErosOrderMessage() throws Exception {
        OrderMessage orderMessage = new OrderMessage();
        orderMessage.setCorrelationReference("abc");
        orderMessage.setExternalReferenceNumber("123");
        orderMessage.setOperator("Raja");
        orderMessage.setReceivingMemberCode("04-1391AA");
        orderMessage.setSendingMemberCode("05-2613AA");
        orderMessage.setService(Service.FTD);
        orderMessage.setOriginalOrderSequenceNumber(0);
        orderMessage.setCardMessage("Generic Card Message.");
        orderMessage.setDeliveryDate(getXMLGregorianCalendarNow());
        orderMessage.setDeliveryDetails("OCT 31 AM");
        orderMessage.setFirstChoiceProductCode("ARR");
        orderMessage.setFirstChoiceProductDescription("First choice flowers");
        orderMessage.setOccasionCode(OccasionCode.ANNIVERSARY);
        orderMessage.setPrice(BigDecimal.TEN);
        orderMessage.setRecipientCity("Mesa");
        orderMessage.setRecipientCountryCode("USA");
        orderMessage.setRecipientName("CANADA ORD 1541019572261");
        orderMessage.setRecipientPhoneNumber("1911111111");
        orderMessage.setRecipientStateCode("AZ");
        orderMessage.setRecipientStreetAddress("123 Test St");
        orderMessage.setSecondChoiceProductCode("ARV");
        orderMessage.setSecondChoiceProductDescription("Second choice flowers.");
        orderMessage.setSpecialInstructions("None");
        orderMessage.setBypassSuspends(false);

        return orderMessage;
    }

    public AskMessage getSampleAskMessage() throws Exception {
        AskMessage askMessage = new AskMessage();
        askMessage.setCorrelationReference("abc");
        askMessage.setExternalReferenceNumber("123");
        askMessage.setOperator("Raja");
        askMessage.setReceivingMemberCode("04-1391AA");
        askMessage.setSendingMemberCode("05-2613AA");
        askMessage.setService(Service.FTD);
        askMessage.setDeliveryDate(getXMLGregorianCalendarNow());
        askMessage.setPrice(BigDecimal.TEN);
        askMessage.setRecipientCity("Mesa");
        askMessage.setRecipientCountryCode("USA");
        askMessage.setRecipientName("CANADA ORD 1541019572261");
        askMessage.setRecipientStateCode("AZ");
        askMessage.setRecipientAddress("123 Test St");
        askMessage.setRelatedOrderSequenceNumber(1);
        askMessage.setRelatedOrderReferenceNumber("J23456");
        askMessage.setReasonText("Ask message");

        return askMessage;
    }

    public AnswerMessage getSampleAnswerMessage() throws Exception {
        AnswerMessage answerMessage = new AnswerMessage();
        answerMessage.setCorrelationReference("abc");
        answerMessage.setExternalReferenceNumber("123");
        answerMessage.setOperator("Raja");
        answerMessage.setReceivingMemberCode("04-1391AA");
        answerMessage.setSendingMemberCode("05-2613AA");
        answerMessage.setService(Service.FTD);
        answerMessage.setDeliveryDate(getXMLGregorianCalendarNow());
        answerMessage.setPrice(BigDecimal.TEN);
        answerMessage.setRecipientCity("Mesa");
        answerMessage.setRecipientCountryCode("USA");
        answerMessage.setRecipientName("CANADA ORD 1541019572261");
        answerMessage.setRecipientStateCode("AZ");
        answerMessage.setRecipientAddress("123 Test St");
        answerMessage.setRelatedOrderSequenceNumber(1);
        answerMessage.setRelatedOrderReferenceNumber("J23456");
        answerMessage.setReasonText("Answer message");

        return answerMessage;
    }

    public RejectOrderMessage getSampleRejectMessage() throws Exception {
        RejectOrderMessage rejectOrderMessage = new RejectOrderMessage();
        rejectOrderMessage.setCorrelationReference("abc");
        rejectOrderMessage.setExternalReferenceNumber("123");
        rejectOrderMessage.setOperator("Raja");
        rejectOrderMessage.setReceivingMemberCode("04-1391AA");
        rejectOrderMessage.setSendingMemberCode("05-2613AA");
        rejectOrderMessage.setService(Service.FTD);
        rejectOrderMessage.setDeliveryDate(getXMLGregorianCalendarNow());
        rejectOrderMessage.setRecipientCity("Mesa");
        rejectOrderMessage.setRecipientCountryCode("USA");
        rejectOrderMessage.setRecipientName("CANADA ORD 1541019572261");
        rejectOrderMessage.setRecipientStateCode("AZ");
        rejectOrderMessage.setRecipientAddress("123 Test St");
        rejectOrderMessage.setRelatedOrderSequenceNumber(1);
        rejectOrderMessage.setRelatedOrderReferenceNumber("J23456");
        rejectOrderMessage.setReasonText("Rejected message");

        return rejectOrderMessage;
    }

    public CancelOrderMessage getSampleCancelOrderMessage() throws Exception {
        CancelOrderMessage cancelOrderMessage = new CancelOrderMessage();
        cancelOrderMessage.setCorrelationReference("abc");
        cancelOrderMessage.setExternalReferenceNumber("123");
        cancelOrderMessage.setOperator("Raja");
        cancelOrderMessage.setReceivingMemberCode("04-1391AA");
        cancelOrderMessage.setSendingMemberCode("05-2613AA");
        cancelOrderMessage.setService(Service.FTD);
        cancelOrderMessage.setDeliveryDate(getXMLGregorianCalendarNow());
        cancelOrderMessage.setRecipientCity("Mesa");
        cancelOrderMessage.setRecipientCountryCode("USA");
        cancelOrderMessage.setRecipientName("CANADA ORD 1541019572261");
        cancelOrderMessage.setRecipientStateCode("AZ");
        cancelOrderMessage.setRecipientAddress("123 Test St");
        cancelOrderMessage.setRelatedOrderSequenceNumber(1);
        cancelOrderMessage.setRelatedOrderReferenceNumber("J23456");
        cancelOrderMessage.setReasonText("Cancel Order message");

        return cancelOrderMessage;
    }

    public ConfirmCancelMessage getSampleConfirmCancelMessage() throws Exception {
        ConfirmCancelMessage confirmCancelMessage = new ConfirmCancelMessage();
        confirmCancelMessage.setCorrelationReference("abc");
        confirmCancelMessage.setExternalReferenceNumber("123");
        confirmCancelMessage.setOperator("Raja");
        confirmCancelMessage.setReceivingMemberCode("04-1391AA");
        confirmCancelMessage.setSendingMemberCode("05-2613AA");
        confirmCancelMessage.setService(Service.FTD);
        confirmCancelMessage.setDeliveryDate(getXMLGregorianCalendarNow());
        confirmCancelMessage.setRecipientCity("Mesa");
        confirmCancelMessage.setRecipientCountryCode("USA");
        confirmCancelMessage.setRecipientName("CANADA ORD 1541019572261");
        confirmCancelMessage.setRecipientStateCode("AZ");
        confirmCancelMessage.setRecipientAddress("123 Test St");
        confirmCancelMessage.setRelatedOrderSequenceNumber(1);
        confirmCancelMessage.setRelatedOrderReferenceNumber("J23456");
        confirmCancelMessage.setReasonText("Confirm Cancel message");

        return confirmCancelMessage;
    }

    public DenyCancelMessage getSampleDenyCancelMessage() throws Exception {
        DenyCancelMessage denyCancelMessage = new DenyCancelMessage();
        denyCancelMessage.setCorrelationReference("abc");
        denyCancelMessage.setExternalReferenceNumber("123");
        denyCancelMessage.setOperator("Raja");
        denyCancelMessage.setReceivingMemberCode("04-1391AA");
        denyCancelMessage.setSendingMemberCode("05-2613AA");
        denyCancelMessage.setService(Service.FTD);
        denyCancelMessage.setDeliveryDate(getXMLGregorianCalendarNow());
        denyCancelMessage.setRecipientCity("Mesa");
        denyCancelMessage.setRecipientCountryCode("USA");
        denyCancelMessage.setRecipientName("CANADA ORD 1541019572261");
        denyCancelMessage.setRecipientStateCode("AZ");
        denyCancelMessage.setRecipientAddress("123 Test St");
        denyCancelMessage.setRelatedOrderSequenceNumber(1);
        denyCancelMessage.setRelatedOrderReferenceNumber("J23456");
        denyCancelMessage.setReasonText("Deny Cancel message");

        return denyCancelMessage;
    }

    public GeneralMessage getSampleGeneralMessage() {
        GeneralMessage generalMessage = new GeneralMessage();
        generalMessage.setCorrelationReference("abc");
        generalMessage.setExternalReferenceNumber("123");
        generalMessage.setOperator("Raja");
        generalMessage.setReceivingMemberCode("04-1391AA");
        generalMessage.setSendingMemberCode("05-2613AA");
        generalMessage.setService(Service.FTD);
        generalMessage.setMessageText("General Message");

        return generalMessage;
    }

    public ForwardMessage getSampleForwardMessage() throws Exception {
        ForwardMessage forwardMessage = new ForwardMessage();
        forwardMessage.setCorrelationReference("abc");
        forwardMessage.setExternalReferenceNumber("123");
        forwardMessage.setOperator("Raja");
        forwardMessage.setReceivingMemberCode("04-1391AA");
        forwardMessage.setSendingMemberCode("05-2613AA");
        forwardMessage.setService(Service.FTD);
        forwardMessage.setDeliveryDate(getXMLGregorianCalendarNow());
        forwardMessage.setNewFillerMemberCode("06-1391AA");
        forwardMessage.setRecipientCity("Mesa");
        forwardMessage.setRecipientCountryCode("USA");
        forwardMessage.setRecipientName("CANADA ORD 1541019572261");
        forwardMessage.setRecipientStateCode("AZ");
        forwardMessage.setRecipientAddress("123 Test St");
        forwardMessage.setRelatedOrderSequenceNumber(1);
        forwardMessage.setRelatedOrderReferenceNumber("J23456");
        forwardMessage.setReasonText("Ask message");

        return forwardMessage;
    }

    private ErosMember getSampleSenderInfo() {
        ErosMember erosMember = new ErosMember();
        erosMember.setMemberCode("05-2613AA");
        erosMember.setAddress("2015 MACDONALD AVE");
        erosMember.setPhone("5102350445");
        erosMember.setCity("Lombard");
        erosMember.setPostalCode("60148");
        erosMember.setStateCode("IL");

        return erosMember;
    }

    private ErosMember getSampleReceiverInfo() {
        ErosMember erosMember = new ErosMember();
        erosMember.setMemberCode("04-1391AA");
        erosMember.setAddress("29941 ALICIA PKY");
        erosMember.setPhone("5102350445");
        erosMember.setCity("Lombard");
        erosMember.setPostalCode("60148");
        erosMember.setStateCode("IL");

        return erosMember;
    }

    private XMLGregorianCalendar getXMLGregorianCalendarNow()
            throws DatatypeConfigurationException {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
        XMLGregorianCalendar now = datatypeFactory
                .newXMLGregorianCalendar(gregorianCalendar);
        return now;
    }
}
