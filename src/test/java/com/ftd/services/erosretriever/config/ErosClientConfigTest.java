package com.ftd.services.erosretriever.config;

import com.ftd.services.erosretriever.BaseApplicationTest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ErosClientConfigTest extends BaseApplicationTest {
    @Mock
    private ErosClientConfig erosClientConfig;

    private Jaxb2Marshaller jaxb2Marshaller;
    private WebServiceTemplate webServiceTemplate;

    @Before
    public void setUp() {
        jaxb2Marshaller = new Jaxb2Marshaller();
        webServiceTemplate = new WebServiceTemplate();
    }

    @Test
    public void testJaxb2Marshaller() {
        Jaxb2Marshaller expected = jaxb2Marshaller;
        when(erosClientConfig.jaxb2Marshaller()).thenReturn(jaxb2Marshaller);
        assertEquals(expected, erosClientConfig.jaxb2Marshaller());
    }

    @Test
    public void testWebServiceTemplate() throws Exception {
        WebServiceTemplate expected = webServiceTemplate;
        when(erosClientConfig.webServiceTemplate()).thenReturn(webServiceTemplate);
        assertEquals(expected, erosClientConfig.webServiceTemplate());
    }
}
