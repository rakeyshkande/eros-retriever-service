package com.ftd.services.erosretriever.config;

import com.ftd.services.erosretriever.BaseApplicationTest;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import static org.mockito.Matchers.isA;

public class SchedulerConfigTest extends BaseApplicationTest {
    @Mock
    private SchedulerConfig schedulerConfig;

    @Test
    public void testConfigureTasks() {
        Mockito.doNothing().when(schedulerConfig).configureTasks(isA(ScheduledTaskRegistrar.class));
    }
}
