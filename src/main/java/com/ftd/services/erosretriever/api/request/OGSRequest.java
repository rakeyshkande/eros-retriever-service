package com.ftd.services.erosretriever.api.request;

import com.ftd.services.erosretriever.dto.OGSOrder;
import lombok.Data;

@Data
public class OGSRequest {

    private OGSOrder order;

}
