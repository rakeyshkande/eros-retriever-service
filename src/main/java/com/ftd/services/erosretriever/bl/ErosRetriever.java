package com.ftd.services.erosretriever.bl;

import com.ftd.services.erosretriever.api.response.ServiceResponse;

public interface ErosRetriever {
    ServiceResponse getNewMessageList(String floristId, String siteId);
    ServiceResponse getMessageDetails(String floristId, Long messageDetailId, String siteId);
}
