package com.ftd.services.erosretriever.bl.impl;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.eapi.message.service.v1.CodeMessagePair;
import com.ftd.eapi.message.service.v1.ErosMessage;
import com.ftd.eapi.message.service.v1.GetMessageDetailByMessageID;
import com.ftd.eapi.message.service.v1.GetNewMessageList;
import com.ftd.eapi.message.service.v1.MessageDetailResponse;
import com.ftd.eapi.message.service.v1.MultiMessageNewListResponse;
import com.ftd.eapi.message.service.v1.NewMessage;
import com.ftd.eapi.message.service.v1.NewMessageList;
import com.ftd.eapi.message.service.v1.NewMessageListResponse;
import com.ftd.eapi.message.service.v1.ObjectFactory;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.SingleMessageResponse;
import com.ftd.services.erosretriever.api.response.ServiceResponse;
import com.ftd.services.erosretriever.bl.ErosRetriever;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.util.ErosSoapClient;
import com.ftd.services.erosretriever.util.InboundMessageHandler;
import com.ftd.services.erosretriever.util.MercuryEapiUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBElement;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

@Slf4j
@Service(value = "erosRetrieverImpl")
public class ErosRetrieverImpl implements ErosRetriever {

    @Autowired
    private ErosSoapClient erosSoapClient;

    @Autowired
    private MercuryEapiUtil mercuryEapiUtil;

    @Autowired
    private InboundMessageHandler inboundMessageHandler;

    @Value("${client.max-results}")
    private Integer maxResults;

    @Override
    @Timed
    @ExceptionMetered
    public ServiceResponse getNewMessageList(String floristId, String siteId) {
        log.info("getNewMessageList(): {} / {}", floristId, siteId);
        ServiceResponse response = new ServiceResponse();

        try {
            boolean moreToProcess = true;
            while (moreToProcess) {
                GetNewMessageList getNewMessageList = new GetNewMessageList();
                getNewMessageList.setMainMemberCode(floristId);
                getNewMessageList.setMaxResults(maxResults);
                ObjectFactory factory = new ObjectFactory();
                JAXBElement<GetNewMessageList> getNewMessageListJAXBElement = factory.createGetNewMessageList(getNewMessageList);
                JAXBElement<NewMessageListResponse> nmlResponseJAXB = (JAXBElement<NewMessageListResponse>) erosSoapClient.call(getNewMessageListJAXBElement, floristId);
                NewMessageListResponse nmlResponse = nmlResponseJAXB.getValue();
                MultiMessageNewListResponse mmnl = nmlResponse.getMultiMessageNewListResponse();

                NewMessageList newMessageList = mmnl.getNewMessageList();
                moreToProcess = false;

                if (newMessageList == null || newMessageList.getNewMessage().isEmpty()) {
                    log.info("no new messages");
                    response.setMessage("no new messages");
                } else {
                    log.info("new messages: {}", newMessageList.getNewMessage().size());
                    for (NewMessage newMessage : newMessageList.getNewMessage()) {
                        log.info(newMessage.getMessageType() + " " + newMessage.getMessageId() + " " + newMessage.getReferenceNumber());
                        boolean success = mercuryEapiUtil.enqueueMessageId(floristId, newMessage.getMessageId(), siteId);
                        if (success) {
                            success = mercuryEapiUtil.confirmMessage(newMessage.getReferenceNumber(), floristId);
                            if (success) {
                                log.info("Confirmed");
                            }
                        }
                    }
                    if (mmnl.isHasMoreMessages()) {
                        log.info("EAPI has more messages");
                        moreToProcess = true;
                    }
                    response.setMessage("Processed Successfully");
                }
            }

        } catch (Exception e) {
            log.error("oops: " + e);
            StringWriter stringWriter = new StringWriter();
            e.printStackTrace(new PrintWriter(stringWriter));
            log.error(stringWriter.toString());
            response.setMessage("Failed while getting new messages from Eros");
        }
        return response;
    }

    @Override
    @Timed
    @ExceptionMetered
    public ServiceResponse getMessageDetails(String floristId, Long messageDetailId, String siteId) {
        log.info("getMessageDetails: {} {}", messageDetailId, siteId);
        ServiceResponse response = new ServiceResponse();
        ProcessedErosMessage message = null;

        try {
            GetMessageDetailByMessageID getMessageDetail = new GetMessageDetailByMessageID();
            getMessageDetail.setMainMemberCode(floristId);
            getMessageDetail.setMessageId(messageDetailId);
            ObjectFactory factory = new ObjectFactory();
            JAXBElement<GetMessageDetailByMessageID> getMessageDetailJAXB = factory.createGetMessageDetailByMessageID(getMessageDetail);
            JAXBElement<MessageDetailResponse> mdResponseJAXB = (JAXBElement<MessageDetailResponse>) erosSoapClient.call(getMessageDetailJAXB, floristId);
            MessageDetailResponse mdResponse = mdResponseJAXB.getValue();
            SingleMessageResponse smResponse = mdResponse.getSingleMessageResponse();
            CodeMessagePair cmp = smResponse.getResponseCode();
            log.info("Response: {} {}", cmp.getCode(), cmp.getMessage());

            message = smResponse.getProcessedErosMessage();
            String adminSequenceNumber = String.format("%04d", message.getAdminSequenceNumber());
            if (Optional.ofNullable(adminSequenceNumber).isPresent()) {
                message.setAdminSequenceNumber(Integer.parseInt(adminSequenceNumber));
            }
            if (message.getOrderSequenceNumber() != null) {
                String orderSequenceNumber = String.format("%04d", message.getOrderSequenceNumber());
                message.setOrderSequenceNumber(Integer.parseInt(orderSequenceNumber));
            }

            ErosMessage erosMessage = message.getErosMessage();
            switch (message.getMessageType()) {
                case GlobalConstants.MESSAGE_TYPE_FTD:
                    inboundMessageHandler.ftdOrder(message, erosMessage, siteId);
                    break;
                case GlobalConstants.MESSAGE_TYPE_CAN:
                    inboundMessageHandler.cancelOrder(message, erosMessage, siteId);
                    break;
                case GlobalConstants.MESSAGE_TYPE_ASK:
                    inboundMessageHandler.askMessage(message, erosMessage, siteId);
                    break;
                case GlobalConstants.MESSAGE_TYPE_ANS:
                    inboundMessageHandler.ansMessage(message, erosMessage, siteId);
                    break;
                case GlobalConstants.MESSAGE_TYPE_REJ:
                    inboundMessageHandler.rejectOrder(message, erosMessage, siteId);
                    break;
                case GlobalConstants.MESSAGE_TYPE_CON:
                    inboundMessageHandler.confirmCancel(message, erosMessage, siteId);
                    break;
                case GlobalConstants.MESSAGE_TYPE_DEN:
                    inboundMessageHandler.denyCancel(message, erosMessage, siteId);
                    break;
                case GlobalConstants.MESSAGE_TYPE_GEN:
                    inboundMessageHandler.generalMessage(message, erosMessage, siteId);
                    break;
                case GlobalConstants.MESSAGE_TYPE_FWD:
                    inboundMessageHandler.forwardMessage(message, erosMessage, siteId);
                    break;
                default:
                    log.error("invalid message type");
            }
            response.setType(GlobalConstants.SUCCESS);
        } catch (Exception e) {
            log.error("error: {}", e);

            /*if (message != null && isNotEmpty(message.getMessageType())) {
                switch (message.getMessageType()) {
                    case GlobalConstants.MESSAGE_TYPE_FTD:
                        orderNotificationFailoverOutboundPublisher.publish(message, pubSubConfig.getPubSub().getOrderGatewayFailoverOutboundTopic());
                        break;
                    default:
                        orderNotificationFailoverOutboundPublisher.publish(message, pubSubConfig.getPubSub().getOrderNotificationFailoverOutboundTopic());
                }
            }*/
        }

        return response;
    }

    public void setErosSoapClient(ErosSoapClient erosSoapClient) {
        this.erosSoapClient = erosSoapClient;
    }

    public void setMercuryEapiUtil(MercuryEapiUtil mercuryEapiUtil) {
        this.mercuryEapiUtil = mercuryEapiUtil;
    }

    public void setInboundMessageHandler(InboundMessageHandler inboundMessageHandler) {
        this.inboundMessageHandler = inboundMessageHandler;
    }
}
