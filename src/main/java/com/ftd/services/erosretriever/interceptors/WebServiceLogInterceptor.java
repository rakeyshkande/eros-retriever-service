package com.ftd.services.erosretriever.interceptors;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Component
@Slf4j
public class WebServiceLogInterceptor implements ClientInterceptor {

    @Override
    public boolean handleRequest(MessageContext messageContext) throws WebServiceClientException {
        WebServiceMessage request = messageContext.getRequest();
        logSoapRequestAndResponse(request, "Request");
        return true;
    }

    @Override
    public boolean handleResponse(MessageContext messageContext) throws WebServiceClientException {
        logSoapRequestAndResponse(messageContext.getResponse(), "Response");
        return true;
    }

    @Override
    public boolean handleFault(MessageContext messageContext) throws WebServiceClientException {
        log.info("endpoint returned a fault");

        return true;
    }

    @Override
    public void afterCompletion(MessageContext messageContext, Exception e) throws WebServiceClientException {
        log.info("Execute Code After Completion");
    }

    private void logSoapRequestAndResponse(WebServiceMessage webServiceMessage, String message) {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            webServiceMessage.writeTo(os);
            log.info("Sent {} [{}]", message, os.toString("UTF-8"));
        } catch (IOException ioException) {
            log.error("Exception occurred while converting SOAP {} to String in WebServiceLogInterceptor: {}", message, ioException.getMessage());
        }
    }
}