package com.ftd.services.erosretriever.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "service")
@EnableConfigurationProperties
public class PubSubConfig {

    private PubSub pubSub;

    public PubSub getPubSub() {
        return pubSub;
    }

    public void setPubSub(PubSub pubSub) {
        this.pubSub = pubSub;
    }

    public static class PubSub {

        private String key;
        private String projectId;
        private long publisherTimeOut;
        private String floristGatewayInboundTopic;
        private String floristGatewayInboundSubscription;
        private String orderGatewayOutboundTopic;
        private String orderNotificationOutboundTopic;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getProjectId() {
            return projectId;
        }

        public void setProjectId(String projectId) {
            this.projectId = projectId;
        }

        public long getPublisherTimeOut() {
            return publisherTimeOut;
        }

        public void setPublisherTimeOut(long publisherTimeOut) {
            this.publisherTimeOut = publisherTimeOut;
        }

        public String getFloristGatewayInboundTopic() {
            return floristGatewayInboundTopic;
        }

        public void setFloristGatewayInboundTopic(String floristGatewayInboundTopic) {
            this.floristGatewayInboundTopic = floristGatewayInboundTopic;
        }

        public String getFloristGatewayInboundSubscription() {
            return floristGatewayInboundSubscription;
        }

        public void setFloristGatewayInboundSubscription(String floristGatewayInboundSubscription) {
            this.floristGatewayInboundSubscription = floristGatewayInboundSubscription;
        }

        public String getOrderGatewayOutboundTopic() {
            return orderGatewayOutboundTopic;
        }

        public void setOrderGatewayOutboundTopic(String orderGatewayOutboundTopic) {
            this.orderGatewayOutboundTopic = orderGatewayOutboundTopic;
        }

        public String getOrderNotificationOutboundTopic() {
            return orderNotificationOutboundTopic;
        }

        public void setOrderNotificationOutboundTopic(String orderNotificationOutboundTopic) {
            this.orderNotificationOutboundTopic = orderNotificationOutboundTopic;
        }
    }
}
