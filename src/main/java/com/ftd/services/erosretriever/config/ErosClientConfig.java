package com.ftd.services.erosretriever.config;

import com.ftd.services.erosretriever.interceptors.WebServiceLogInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

@Configuration
public class ErosClientConfig {

    @Value("${client.default-uri}")
    private String defaultUri;

    @Value("${client.context-package:com.ftd.eapi.message.service.v1}")
    private String contextPackage;

    @Value("${eapiConnection.timeout.connect}")
    private int connectionTimeOut;

    @Value("${eapiConnection.timeout.socket}")
    private int socketTimeout;

    @Value("${eapiConnection.max:100}")
    private int maxConnections;

    @Bean
    public Jaxb2Marshaller jaxb2Marshaller() {
        Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
        jaxb2Marshaller.setContextPath(contextPackage);
        return jaxb2Marshaller;
    }

    @Bean
    public WebServiceLogInterceptor webServiceLogInterceptor() {
        WebServiceLogInterceptor webServiceLogInterceptor = new WebServiceLogInterceptor();
        return webServiceLogInterceptor;
    }

    @Bean
    public WebServiceTemplate webServiceTemplate() {
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
        webServiceTemplate.setMarshaller(jaxb2Marshaller());
        webServiceTemplate.setUnmarshaller(jaxb2Marshaller());
        ClientInterceptor[] interceptors = {webServiceLogInterceptor()};
        webServiceTemplate.setInterceptors(interceptors);
        webServiceTemplate.setDefaultUri(defaultUri);

        return webServiceTemplate;
    }

    @Bean
    public WebServiceMessageSender webServiceMessageSender() {
        HttpComponentsMessageSender httpComponentsMessageSender = new HttpComponentsMessageSender();
        httpComponentsMessageSender.setMaxTotalConnections(maxConnections);
        // timeout for creating a connection
        httpComponentsMessageSender.setConnectionTimeout(connectionTimeOut);
        // when you have a connection, timeout the read blocks for
        httpComponentsMessageSender.setReadTimeout(socketTimeout);
        return httpComponentsMessageSender;
    }

}
