package com.ftd.services.erosretriever.dto;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaxAmounts {
    private String taxType;
    private String taxDescription;
    private BigDecimal taxRate;
    private BigDecimal taxAmount;
}