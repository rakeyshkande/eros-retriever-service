package com.ftd.services.erosretriever.dto;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Accessories {
    private String accessoryId;
    private BigDecimal accessoryPrice;
    private Integer accessoryQty;
}