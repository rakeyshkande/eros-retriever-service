package com.ftd.services.erosretriever.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FeeCharges {
    private String name;
    private String type;
    private String feeType;
    private BigDecimal value;
    private String valueType;
}