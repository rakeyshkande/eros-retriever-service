package com.ftd.services.erosretriever.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApplicableCharges {
    private List<FeeCharges> surCharges;
    private List<FeeCharges> deliveryCharges;
    private BigDecimal totalApplicableCharges;
}