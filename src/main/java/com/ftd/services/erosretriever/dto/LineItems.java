package com.ftd.services.erosretriever.dto;

import java.util.List;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.validation.constraints.Pattern;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LineItems {
    private String lineItemId;
    private String productId;
    private String websiteId;
    @Pattern(regexp = "florist|dropship", flags = Pattern.Flag.CASE_INSENSITIVE) private String fulfillmentChannel;
    @Pattern(regexp = "Florist Delivered|Next-Day|2-Day|Standard|Saturday|Sunday", flags = Pattern.Flag.CASE_INSENSITIVE) private String serviceLevel;
    @Pattern(regexp = "Morning Delivery", flags = Pattern.Flag.CASE_INSENSITIVE) private String dayOfDeliveryServiceLevel;
    @Pattern(regexp = "floral|freshCut|sameDayFreshCut|service|specialtyGift", flags = Pattern.Flag.CASE_INSENSITIVE) private String productType;
    @Pattern(regexp = "Domestic|International", flags = Pattern.Flag.CASE_INSENSITIVE) private String deliveryType;
    private boolean freeShippingFlag;
    private CodeList codeList;
    private List<Accessories> accessories;
    private List<NameValue> lineItemAmounts;
    private Fees fees;
    private List<TaxAmounts> taxAmounts;
}