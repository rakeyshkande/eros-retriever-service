package com.ftd.services.erosretriever.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Fees {
    private ApplicableCharges applicableCharges;
    private DiscountedCharges discountedCharges;
}