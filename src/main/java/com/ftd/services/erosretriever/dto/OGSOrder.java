package com.ftd.services.erosretriever.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OGSOrder {

    private String orderId;
    private String orderChannel;
    private String orderType;
    private String orderReferenceId;
    private String orderTimestamp;
    private String languageId;
    private CodeList codeList;
    private List<NameValue> fraud;
    private BuyerDetails buyerDetails;
    private PaymentDetails paymentDetails;
    private List<NameValue> orderAmounts;
    private List<DeliveryGroups> deliveryGroups;

}
