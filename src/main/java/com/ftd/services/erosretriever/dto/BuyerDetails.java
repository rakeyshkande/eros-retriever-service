package com.ftd.services.erosretriever.dto;

import java.util.List;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

import javax.validation.constraints.Pattern;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BuyerDetails {
    private String profileId;
    private String firstName;
    private String lastName;
    private String businessName;
    private String middleName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postalCode;
    private String countryCode;
    private String emailAddress;
    @Pattern(regexp = "Account|Guest", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String profileType;
    private boolean emailOptIn;
    private String birthday;
    private List<PhoneNumbers> phoneNumbers;
}