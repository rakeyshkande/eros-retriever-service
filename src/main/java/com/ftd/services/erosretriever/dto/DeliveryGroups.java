package com.ftd.services.erosretriever.dto;

import java.util.List;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryGroups {
    private String deliveryGroupId;
    private String deliveryDate;
    private String deliveryDateEnd;
    private String occasion;
    private String giftMessage;
    private RecipientDetails recipientDetails;
    private List<LineItems> lineItems;
}