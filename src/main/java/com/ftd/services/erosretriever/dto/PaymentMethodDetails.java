package com.ftd.services.erosretriever.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = {"creditCardNumber", "creditCardExpireMonth", "creditCardExpireYear", "gcNumber", "threeDSecure", "authorizationDetails"})
public class PaymentMethodDetails {
    private String creditCardType;
    private String creditCardNumber;
    private String creditCardExpireMonth;
    private String creditCardExpireYear;
    private boolean creditCardEncrypted;
    private String tokenId;
    private String paymentType;
    private String accountNumber;
    private String gcNumber;
    private BigDecimal gcAmount;
    private List<NameValue> threeDSecure;
    private List<NameValue> authorizationDetails;
}