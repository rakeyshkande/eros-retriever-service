package com.ftd.services.erosretriever.dto;

import java.util.List;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RecipientDetails {
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String postalCode;
    private String countryCode;
    private String addressType;
    private String addressTypeName;
    private String addressTypeInfo;
    private List<PhoneNumbers> phoneNumbers;
    private List<NameValue> addressVerification;
}