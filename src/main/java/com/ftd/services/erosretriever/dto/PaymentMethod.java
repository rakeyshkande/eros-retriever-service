package com.ftd.services.erosretriever.dto;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentMethod {
    private String paymentMethodType;
    private String authorizationTransactionId;
    private String merchantReferenceId;
    private String authorizationRoute;
    private BuyerDetails billingInformation;
    private PaymentMethodDetails paymentMethodDetails;
}