package com.ftd.services.erosretriever.dto;

import java.util.List;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NameValue {
    private String name;
    private String value;
    private List<NameValue> map;
}