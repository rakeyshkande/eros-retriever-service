package com.ftd.services.erosretriever.dto;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PhoneNumbers {
    private String phoneType;
    private String phoneNumber;
    private String phoneExt;
    private boolean smsOptIn;
}