package com.ftd.services.erosretriever.resource;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.commons.logging.annotation.LogExecutionTime;
import com.ftd.services.erosretriever.api.response.ServiceResponse;
import com.ftd.services.erosretriever.bl.ErosRetriever;
import com.ftd.services.erosretriever.config.FloristProperties;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/{siteId}/api")
public class ServiceResource {

    @Autowired
    private ErosRetriever erosRetriever;

    @Autowired
    private FloristProperties floristProperties;

    @RequestMapping(value = "/florists/{floristId}/processFloristMessages")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Process Florist Messages", notes = "Process Florist Messages by siteId and FloristId",
            response = ServiceResponse.class, responseContainer = "Class", responseReference = "ServiceResponse")
    @ApiResponses(value = {
            @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "Successfully processed florist messages"),
            @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "Bad Request - Failed processing florist messages"),
            @ApiResponse(code = HttpURLConnection.HTTP_FORBIDDEN, message = "Accessing this resource is forbidden")})
    @Timed
    @ExceptionMetered
    @LogExecutionTime
    public ServiceResponse processFloristMessages(@PathVariable("siteId") String siteId, @PathVariable("floristId") String floristId) {
        log.info("Process New Messages for floristId: {} and siteId: {}", floristId, siteId);
        return erosRetriever.getNewMessageList(floristId, siteId);
    }

    @RequestMapping(value = "/getMessageDetails")
    @ApiImplicitParam(name = "siteId",
            value = "Valid examples siteId=[proflowers, ftd]",
            required = true,
            dataType = "string",
            paramType = "query")
    @Timed
    @ExceptionMetered
    @LogExecutionTime
    public ServiceResponse getMessageDetails(@RequestParam(value = "floristId") String floristId,
            @RequestParam(value = "messageDetailId") long messageDetailId, @PathVariable String siteId) {
        log.info("getMessageDetails: {} {} {}", floristId, messageDetailId, siteId);
        return erosRetriever.getMessageDetails(floristId, messageDetailId, siteId);
    }

    @RequestMapping(value = "/processAllFloristMessages")
    @ApiImplicitParam(name = "siteId",
            value = "Valid examples siteId=[proflowers, ftd]",
            required = true)
    @Timed
    @ExceptionMetered
    @LogExecutionTime
    public void processFloristMessages() {
        log.info("Start Process Florist Messages");
        List<FloristProperties.FloristConfiguration> florists = floristProperties.getFloristConfiguration();
        if (Optional.ofNullable(florists).isPresent()) {
            for (FloristProperties.FloristConfiguration fc : florists) {
                erosRetriever.getNewMessageList(fc.getFloristId(), fc.getSiteId());
            }
        }
        log.info("End Process Florist Messages");
    }
}


