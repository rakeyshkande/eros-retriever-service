package com.ftd.services.erosretriever.event.publisher;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FailoverOutboundPublisher {
    @Autowired
    private PublisherUtil publisherUtil;

    public void publish(ProcessedErosMessage processedErosMessage, String topicName) {
        Gson gson = new Gson();
        String message = gson.toJson(processedErosMessage);
        publisherUtil.publish(message, topicName);
        log.info("Event has been published with message {} to {} topic.", message, topicName);
    }
}
