package com.ftd.services.erosretriever.event.subscriber.internal;

import com.ftd.commons.pubsub.util.AbstractSubscriber;
import com.ftd.services.erosretriever.api.response.ServiceResponse;
import com.ftd.services.erosretriever.bl.ErosRetriever;
import com.ftd.services.erosretriever.domain.event.FloristInboundEvent;
import com.google.gson.Gson;
import com.google.pubsub.v1.PubsubMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component("floristGatewayInboundSubscriber")
@Slf4j
public class FloristGatewayInboundSubscriber extends AbstractSubscriber {

    @Autowired
    private ErosRetriever erosRetriever;

    @Override
    public Status onSubscribe(PubsubMessage message) {
        String body = message.getData().toStringUtf8();
        log.info("FloristGatewayInboundSubscriber received message : {}", body);

        try {
            Gson gson = new Gson();
            FloristInboundEvent event = gson.fromJson(body, FloristInboundEvent.class);
            ServiceResponse response = erosRetriever.getMessageDetails(event.getFloristId(),
                    event.getMessageId(), event.getSiteId().toString());
            log.info("ServiceResponse: {}", response.getType());
            return Status.SUCCESS;
        } catch (Exception ioe) {
            log.error("exception: {}", ioe.getMessage());
        }
        return null;
    }
}
