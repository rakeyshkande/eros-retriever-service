package com.ftd.services.erosretriever.event.publisher.internal;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.services.erosretriever.config.PubSubConfig;
import com.ftd.services.erosretriever.domain.event.FloristInboundEvent;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FloristGatewayInboundPublisher {

    @Autowired
    private PublisherUtil publisherUtil;

    @Autowired
    private PubSubConfig pubSubConfig;

    public void publish(FloristInboundEvent floristInboundEvent) {
        Gson gson = new Gson();
        String message = gson.toJson(floristInboundEvent);
        String floristGatewayInboundTopic = pubSubConfig.getPubSub().getFloristGatewayInboundTopic();
        publisherUtil.publish(message, floristGatewayInboundTopic);
        log.info("Event has been published with message {} to {} topic.", message, floristGatewayInboundTopic);
    }
}
