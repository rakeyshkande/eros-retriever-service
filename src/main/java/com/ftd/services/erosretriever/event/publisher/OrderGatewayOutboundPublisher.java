package com.ftd.services.erosretriever.event.publisher;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.services.erosretriever.api.request.OGSRequest;
import com.ftd.services.erosretriever.config.PubSubConfig;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderGatewayOutboundPublisher {

    @Autowired
    private PublisherUtil publisherUtil;

    @Autowired
    private PubSubConfig pubSubConfig;

    public void publish(OGSRequest ogsRequest) {
        Gson gson = new Gson();
        String message = gson.toJson(ogsRequest);
        String orderGatewayOutboundTopic = pubSubConfig.getPubSub().getOrderGatewayOutboundTopic();
        publisherUtil.publish(message, orderGatewayOutboundTopic);
        log.info("Event has been published with message {} to {} topic.", message, orderGatewayOutboundTopic);
    }
}
