package com.ftd.services.erosretriever.event.publisher;

import com.ftd.commons.pubsub.util.PublisherUtil;
import com.ftd.services.erosretriever.config.PubSubConfig;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderNotificationOutboundPublisher {

    @Autowired
    private PublisherUtil publisherUtil;

    @Autowired
    private PubSubConfig pubSubConfig;

    public void publish(FloristMessageNotification floristMessageNotification) {
        Gson gson = new Gson();
        String message = gson.toJson(floristMessageNotification);
        String orderNotificationOutboundTopic = pubSubConfig.getPubSub().getOrderNotificationOutboundTopic();
        publisherUtil.publish(message, orderNotificationOutboundTopic);
        log.info("Event has been published with message {} to {} topic.", message, orderNotificationOutboundTopic);
    }
}
