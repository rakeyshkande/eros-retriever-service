package com.ftd.services.erosretriever.transformer.impl;

import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.RejectOrderMessage;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.Transformer;
import com.ftd.services.erosretriever.types.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.ftd.services.erosretriever.util.TransformerUtil.populateOrderRelatedMessageInfo;

@Component
@Slf4j
public class RejectOrderMessageTransformer implements Transformer<ProcessedErosMessage, RejectOrderMessage, FloristMessageNotification> {

    @Override
    public FloristMessageNotification transform(ProcessedErosMessage processedErosMessage, RejectOrderMessage rejectOrderMessage) {
        log.info("Transform RejectOrderMessage to FloristMessageNotification");
        return FloristMessageNotification.builder()
                .data(populateOrderRelatedMessageInfo(processedErosMessage, rejectOrderMessage))
                .eventName(EventType.REJECT_ORDER)
                .source(GlobalConstants.SOURCE)
                .fulfillmentChannel(GlobalConstants.FULFILLMENT_CHANNEL)
                .build();
    }
}
