package com.ftd.services.erosretriever.transformer.impl;

import com.ftd.eapi.message.service.v1.AnswerMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.Data;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.Transformer;
import com.ftd.services.erosretriever.types.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.ftd.services.erosretriever.util.TransformerUtil.populateOrderRelatedMessageInfo;

@Component
@Slf4j
public class AnswerMessageTransformer implements Transformer<ProcessedErosMessage, AnswerMessage, FloristMessageNotification> {

    @Override
    public FloristMessageNotification transform(ProcessedErosMessage processedErosMessage, AnswerMessage answerMessage) {
        log.info("Transform AnswerMessage to FloristMessageNotification");
        Data data = populateOrderRelatedMessageInfo(processedErosMessage, answerMessage);
        if (Optional.ofNullable(answerMessage.getRequestCode()).isPresent()) {
            data.setRequestCode(answerMessage.getRequestCode().name());
        }
        data.setPrice(answerMessage.getPrice());

        return FloristMessageNotification.builder()
                .data(data)
                .fulfillmentChannel(GlobalConstants.FULFILLMENT_CHANNEL)
                .source(GlobalConstants.SOURCE)
                .eventName(EventType.FULFILLMENT_COMMUNICATION)
                .build();
    }
}
