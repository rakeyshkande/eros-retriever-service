package com.ftd.services.erosretriever.transformer;

public interface Transformer<S, T, U> {

    U transform(S common, T message);
}
