package com.ftd.services.erosretriever.transformer.impl;

import com.ftd.eapi.message.service.v1.AskMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.Data;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.Transformer;
import com.ftd.services.erosretriever.types.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

import static com.ftd.services.erosretriever.util.TransformerUtil.populateOrderRelatedMessageInfo;

@Component
@Slf4j
public class AskMessageTransformer implements Transformer<ProcessedErosMessage, AskMessage, FloristMessageNotification> {

    @Override
    public FloristMessageNotification transform(ProcessedErosMessage processedErosMessage, AskMessage askMessage) {
        log.info("Transform AskMessage to FloristMessageNotification");
        Data data = populateOrderRelatedMessageInfo(processedErosMessage, askMessage);
        if (Optional.ofNullable(askMessage.getRequestCode()).isPresent()) {
            data.setRequestCode(askMessage.getRequestCode().name());
        }
        data.setPrice(askMessage.getPrice());

        return FloristMessageNotification.builder()
                .data(data)
                .fulfillmentChannel(GlobalConstants.FULFILLMENT_CHANNEL)
                .source(GlobalConstants.SOURCE)
                .eventName(EventType.FULFILLMENT_COMMUNICATION)
                .build();
    }
}
