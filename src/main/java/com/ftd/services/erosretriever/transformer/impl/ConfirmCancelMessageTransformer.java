package com.ftd.services.erosretriever.transformer.impl;

import com.ftd.eapi.message.service.v1.ConfirmCancelMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.Transformer;
import com.ftd.services.erosretriever.types.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.ftd.services.erosretriever.util.TransformerUtil.populateOrderRelatedMessageInfo;

@Component
@Slf4j
public class ConfirmCancelMessageTransformer implements Transformer<ProcessedErosMessage, ConfirmCancelMessage, FloristMessageNotification> {
    @Override
    public FloristMessageNotification transform(ProcessedErosMessage processedErosMessage, ConfirmCancelMessage confirmCancelMessage) {
        log.info("Transform ConfirmCancelMessage to FloristMessageNotification");
        return FloristMessageNotification.builder()
                .data(populateOrderRelatedMessageInfo(processedErosMessage, confirmCancelMessage))
                .fulfillmentChannel(GlobalConstants.FULFILLMENT_CHANNEL)
                .source(GlobalConstants.SOURCE)
                .eventName(EventType.FULFILLMENT_COMMUNICATION)
                .build();
    }
}
