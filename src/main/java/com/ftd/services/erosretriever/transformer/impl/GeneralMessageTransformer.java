package com.ftd.services.erosretriever.transformer.impl;

import com.ftd.eapi.message.service.v1.GeneralMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.Data;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.Transformer;
import com.ftd.services.erosretriever.types.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.ftd.services.erosretriever.util.TransformerUtil.checkNotNullAndSetData;

@Component
@Slf4j
public class GeneralMessageTransformer implements Transformer<ProcessedErosMessage, GeneralMessage, FloristMessageNotification> {
    @Override
    public FloristMessageNotification transform(ProcessedErosMessage processedErosMessage, GeneralMessage generalMessage) {
        log.info("Transform GeneralMessage to FloristMessageNotification");
        Data data = Data.builder()
                .externalReferenceNumber(generalMessage.getExternalReferenceNumber())
                .operator(generalMessage.getOperator())
                .receivingMemberCode(generalMessage.getReceivingMemberCode())
                .sendingMemberCode(generalMessage.getSendingMemberCode())
                .service(generalMessage.getService().value())
                .correlationReference(generalMessage.getCorrelationReference())
                .messageText(generalMessage.getMessageText())
                .priority(generalMessage.getPriority())
                .slimcast(generalMessage.isSlimcast())
                .adminSequenceNumber(processedErosMessage.getAdminSequenceNumber())
                .error(processedErosMessage.getError())
                .messageId(processedErosMessage.getMessageId())
                .messageStatus(processedErosMessage.getMessageStatus())
                .messageType(processedErosMessage.getMessageType())
                .relatedMessageId(processedErosMessage.getRelatedMessageId())
                .orderSequenceNumber(processedErosMessage.getOrderSequenceNumber())
                .transmissionId(processedErosMessage.getTransmissionId())
                .transmissionStatus(processedErosMessage.getTransmissionStatus())
                .warning(processedErosMessage.getWarning())
                .build();

        checkNotNullAndSetData(processedErosMessage, data);

        return FloristMessageNotification.builder()
                .data(data)
                .eventName(EventType.FULFILLMENT_COMMUNICATION)
                .fulfillmentChannel(GlobalConstants.FULFILLMENT_CHANNEL)
                .source(GlobalConstants.SOURCE)
                .build();
    }
}
