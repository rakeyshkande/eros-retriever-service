package com.ftd.services.erosretriever.transformer.impl;

import com.ftd.eapi.message.service.v1.CancelOrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.Transformer;
import com.ftd.services.erosretriever.types.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.ftd.services.erosretriever.util.TransformerUtil.populateOrderRelatedMessageInfo;

@Component
@Slf4j
public class CancelOrderMessageTransformer implements Transformer<ProcessedErosMessage, CancelOrderMessage, FloristMessageNotification> {

    @Override
    public FloristMessageNotification transform(ProcessedErosMessage processedErosMessage, CancelOrderMessage cancelOrderMessage) {
        log.info("Transform CancelOrderMessage to FloristMessageNotification");
        return FloristMessageNotification.builder()
                .data(populateOrderRelatedMessageInfo(processedErosMessage, cancelOrderMessage))
                .fulfillmentChannel(GlobalConstants.FULFILLMENT_CHANNEL)
                .source(GlobalConstants.SOURCE)
                .eventName(EventType.FULFILLMENT_COMMUNICATION)
                .build();
    }
}
