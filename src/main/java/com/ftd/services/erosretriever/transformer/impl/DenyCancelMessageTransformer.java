package com.ftd.services.erosretriever.transformer.impl;

import com.ftd.eapi.message.service.v1.DenyCancelMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.Transformer;
import com.ftd.services.erosretriever.types.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.ftd.services.erosretriever.util.TransformerUtil.populateOrderRelatedMessageInfo;

@Component
@Slf4j
public class DenyCancelMessageTransformer implements Transformer<ProcessedErosMessage, DenyCancelMessage, FloristMessageNotification> {
    @Override
    public FloristMessageNotification transform(ProcessedErosMessage processedErosMessage, DenyCancelMessage denyCancelMessage) {
        log.info("Transform DenyCancelMessage to FloristMessageNotification");
        return FloristMessageNotification.builder()
                .data(populateOrderRelatedMessageInfo(processedErosMessage, denyCancelMessage))
                .source(GlobalConstants.SOURCE)
                .fulfillmentChannel(GlobalConstants.FULFILLMENT_CHANNEL)
                .eventName(EventType.FULFILLMENT_COMMUNICATION)
                .build();
    }
}
