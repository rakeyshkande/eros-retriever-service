package com.ftd.services.erosretriever.transformer.impl;

import com.ftd.eapi.message.service.v1.ForwardMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.Data;
import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.transformer.Transformer;
import com.ftd.services.erosretriever.types.EventType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import static com.ftd.services.erosretriever.util.TransformerUtil.populateOrderRelatedMessageInfo;

@Component
@Slf4j
public class ForwardMessageTransformer implements Transformer<ProcessedErosMessage, ForwardMessage, FloristMessageNotification> {
    @Override
    public FloristMessageNotification transform(ProcessedErosMessage processedErosMessage, ForwardMessage forwardMessage) {
        log.info("Transform ForwardMessage to FloristMessageNotification");
        Data data = populateOrderRelatedMessageInfo(processedErosMessage, forwardMessage);
        data.setNewFillerListingCode(forwardMessage.getNewFillerListingCode());
        data.setNewFillerMemberCode(forwardMessage.getNewFillerMemberCode());

        return FloristMessageNotification.builder()
                .data(data)
                .fulfillmentChannel(GlobalConstants.FULFILLMENT_CHANNEL)
                .source(GlobalConstants.SOURCE)
                .eventName(EventType.FULFILLMENT_COMMUNICATION)
                .build();
    }
}
