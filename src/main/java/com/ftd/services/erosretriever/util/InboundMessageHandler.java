package com.ftd.services.erosretriever.util;

import com.ftd.eapi.message.service.v1.AnswerMessage;
import com.ftd.eapi.message.service.v1.AskMessage;
import com.ftd.eapi.message.service.v1.CancelOrderMessage;
import com.ftd.eapi.message.service.v1.ConfirmCancelMessage;
import com.ftd.eapi.message.service.v1.DenyCancelMessage;
import com.ftd.eapi.message.service.v1.ErosMessage;
import com.ftd.eapi.message.service.v1.ForwardMessage;
import com.ftd.eapi.message.service.v1.GeneralMessage;
import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.eapi.message.service.v1.RejectOrderMessage;
import com.ftd.services.erosretriever.service.OrderGatewayService;
import com.ftd.services.erosretriever.service.OrderService;
import com.ftd.services.erosretriever.transformer.impl.AnswerMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.AskMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.CancelOrderMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.ConfirmCancelMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.DenyCancelMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.ForwardMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.GeneralMessageTransformer;
import com.ftd.services.erosretriever.transformer.impl.RejectOrderMessageTransformer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class InboundMessageHandler {

    @Autowired
    private OrderGatewayService orderGatewayService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private CancelOrderMessageTransformer cancelOrderMessageTransformer;

    @Autowired
    private AskMessageTransformer askMessageTransformer;

    @Autowired
    private AnswerMessageTransformer answerMessageTransformer;

    @Autowired
    private RejectOrderMessageTransformer rejectOrderMessageTransformer;

    @Autowired
    private GeneralMessageTransformer generalMessageTransformer;

    @Autowired
    private ConfirmCancelMessageTransformer confirmCancelMessageTransformer;

    @Autowired
    private DenyCancelMessageTransformer denyCancelMessageTransformer;

    @Autowired
    private ForwardMessageTransformer forwardMessageTransformer;

    public void ftdOrder(ProcessedErosMessage message, ErosMessage erosMessage, String siteId) {
        String sequenceKey = String.format("%04d", message.getAdminSequenceNumber());
        String mercuryMessageNumber = message.getReferenceNumber() + "-" + sequenceKey;
        message.setNotification(mercuryMessageNumber);

        OrderMessage order = (OrderMessage) erosMessage;
        log.info(message.getNotification() + " " + order.getRecipientName());
        orderGatewayService.processWebgiftOrder(order, message, siteId);
    }

    public void cancelOrder(ProcessedErosMessage message, ErosMessage erosMessage, String siteId) {
        CancelOrderMessage cancelOrderMessage = (CancelOrderMessage) erosMessage;
        setNotification(message, cancelOrderMessage.getRelatedOrderSequenceNumber(), cancelOrderMessage.getRelatedOrderReferenceNumber());
        log.info(message.getNotification() + " " + cancelOrderMessage.getReasonText() + " " + siteId);
        orderService.orderNotification(cancelOrderMessageTransformer.transform(message, cancelOrderMessage));
    }

    public void askMessage(ProcessedErosMessage message, ErosMessage erosMessage, String siteId) {
        AskMessage askMessage = (AskMessage) erosMessage;
        setNotification(message, askMessage.getRelatedOrderSequenceNumber(), askMessage.getRelatedOrderReferenceNumber());
        log.info(message.getNotification() + " " + askMessage.getReasonText() + " " + siteId);
        orderService.orderNotification(askMessageTransformer.transform(message, askMessage));
    }

    public void ansMessage(ProcessedErosMessage message, ErosMessage erosMessage, String siteId) {
        AnswerMessage answerMessage = (AnswerMessage) erosMessage;
        setNotification(message, answerMessage.getRelatedOrderSequenceNumber(), answerMessage.getRelatedOrderReferenceNumber());
        log.info(message.getNotification() + " " + answerMessage.getReasonText() + " " + siteId);
        orderService.orderNotification(answerMessageTransformer.transform(message, answerMessage));
    }

    public void rejectOrder(ProcessedErosMessage message, ErosMessage erosMessage, String siteId) {
        RejectOrderMessage rejectOrderMessage = (RejectOrderMessage) erosMessage;
        setNotification(message, rejectOrderMessage.getRelatedOrderSequenceNumber(), rejectOrderMessage.getRelatedOrderReferenceNumber());
        log.info(message.getNotification() + " " + rejectOrderMessage.getReasonText() + " " + siteId);
        orderService.orderNotification(rejectOrderMessageTransformer.transform(message, rejectOrderMessage));
    }

    public void generalMessage(ProcessedErosMessage message, ErosMessage erosMessage, String siteId) {
        GeneralMessage generalMessage = (GeneralMessage) erosMessage;
        //TODO - we are setting notification admin sequenceNumber and referenceNumber
        setNotification(message, message.getAdminSequenceNumber(), message.getReferenceNumber());
        log.info(siteId + " " + message.getNotification() + " " + generalMessage.getMessageText());
        orderService.orderNotification(generalMessageTransformer.transform(message, generalMessage));
    }

    public void confirmCancel(ProcessedErosMessage message, ErosMessage erosMessage, String siteId) {
        ConfirmCancelMessage confirmCancelMessage = (ConfirmCancelMessage) erosMessage;
        setNotification(message, confirmCancelMessage.getRelatedOrderSequenceNumber(), confirmCancelMessage.getRelatedOrderReferenceNumber());
        log.info(message.getNotification() + " " + confirmCancelMessage.getReasonText() + " " + siteId);
        orderService.orderNotification(confirmCancelMessageTransformer.transform(message, confirmCancelMessage));
    }

    public void denyCancel(ProcessedErosMessage message, ErosMessage erosMessage, String siteId) {
        DenyCancelMessage denyCancelMessage = (DenyCancelMessage) erosMessage;
        setNotification(message, denyCancelMessage.getRelatedOrderSequenceNumber(), denyCancelMessage.getRelatedOrderReferenceNumber());
        log.info(message.getNotification() + " " + denyCancelMessage.getReasonText() + " " + siteId);
        orderService.orderNotification(denyCancelMessageTransformer.transform(message, denyCancelMessage));
    }

    public void forwardMessage(ProcessedErosMessage message, ErosMessage erosMessage, String siteId) {
        ForwardMessage forwardMessage = (ForwardMessage) erosMessage;
        setNotification(message, forwardMessage.getRelatedOrderSequenceNumber(), forwardMessage.getRelatedOrderReferenceNumber());
        log.info(message.getNotification() + " " + forwardMessage.getReasonText() + " " + siteId);
        orderService.orderNotification(forwardMessageTransformer.transform(message, forwardMessage));
    }

    private void setNotification(ProcessedErosMessage message, Integer sequenceNumber, String refNumber) {
        String seqNumber = String.format("%04d", sequenceNumber);
        String orderId = refNumber + "-" + seqNumber;
        message.setNotification(orderId);
    }
}
