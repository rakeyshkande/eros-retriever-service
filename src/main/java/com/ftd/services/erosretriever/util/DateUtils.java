package com.ftd.services.erosretriever.util;

import lombok.extern.slf4j.Slf4j;

import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class DateUtils {


    /*
     * Converts XMLGregorianCalendar to java.util.Date in Java
     */
    public static Date toDate(XMLGregorianCalendar calendar) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        if (calendar == null) {
            return null;
        }
        Date toDate = calendar.toGregorianCalendar().getTime();
        try {
            toDate = simpleDateFormat.parse(simpleDateFormat.format(calendar.toGregorianCalendar().getTime()));
        } catch (Exception e) {
            log.error("Could not parse XMLGregorianCalendar date" + e);
        }
        return toDate;
    }

    public static Date toDateTime(XMLGregorianCalendar calendar) {
        if (calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }
}
