package com.ftd.services.erosretriever.util;

import com.ftd.services.erosretriever.config.FloristProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.xml.transform.StringSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.List;

@Component
@Slf4j
public class ErosSoapClient {

    @Autowired
    private FloristProperties floristProperties;

    @Value("${client.default-uri}")
    private String defaultUri;

    @Value("${client.xsd-wsse}")
    private String xsdWSSE;

    @Value("${client.xsd-wsse-password}")
    private String xsdWSSEPassword;

    private String userName = null;
    private String password = null;

    @Autowired
    private WebServiceTemplate webServiceTemplate;

    public Object call(Object input, String floristId) {
        List<FloristProperties.FloristConfiguration> florists = floristProperties.getFloristConfiguration();
        if (florists != null) {
            for (FloristProperties.FloristConfiguration fc : florists) {
                if (fc.getFloristId().equals(floristId)) {
                    userName = fc.getUserId();
                    password = fc.getPassword();
                    break;
                }
            }
        }

        return marshalSendAndReceive(input);
    }

    public Object marshalSendAndReceive(Object input) {
        return webServiceTemplate.marshalSendAndReceive(
                input, message -> {
                    try {
                        SoapMessage soapMessage = (SoapMessage) message;
                        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                        Document doc = dBuilder.newDocument();

                        Element elementSecurity = doc.createElement("Security");
                        elementSecurity.setAttribute("xmlns", xsdWSSE);
                        doc.appendChild(elementSecurity);

                        Element elementCredentials = doc.createElement("UsernameToken");
                        Element elementUser = doc.createElement("Username");
                        elementUser.setTextContent(userName);
                        Element elementPassword = doc.createElement("Password");
                        elementPassword.setAttribute("Type", xsdWSSEPassword);
                        elementPassword.setTextContent(password);
                        elementCredentials.appendChild(elementUser);
                        elementCredentials.appendChild(elementPassword);
                        elementSecurity.appendChild(elementCredentials);

                        DOMSource domSource = new DOMSource(doc);
                        StringWriter writer = new StringWriter();
                        StreamResult result = new StreamResult(writer);
                        Transformer transformer = TransformerFactory.newInstance().newTransformer();
                        transformer.transform(domSource, result);
                        String docString = writer.toString();

                        SoapHeader header = soapMessage.getSoapHeader();
                        StringSource headerSource = new StringSource(docString);
                        transformer.transform(headerSource, header.getResult());

                    } catch (Exception e) {
                        log.error("error during marshalling of the SOAP headers", e);
                    }
                });
    }

    public void setWebServiceTemplate(final WebServiceTemplate template) {
        this.webServiceTemplate = template;
    }

    public void setFloristProperties(FloristProperties floristProperties) {
        this.floristProperties = floristProperties;
    }
}
