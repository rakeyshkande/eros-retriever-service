package com.ftd.services.erosretriever.util;

import com.ftd.services.erosretriever.bl.ErosRetriever;
import com.ftd.services.erosretriever.config.FloristProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class Scheduler {

    @Autowired
    private ErosRetriever erosRetriever;

    @Autowired
    private FloristProperties floristProperties;

    //@Scheduled(fixedDelayString = "${configurations.scheduleDelay}", initialDelay = 10000)
    public void scheduleTaskWithFixedDelay() {
        List<FloristProperties.FloristConfiguration> florists = floristProperties.getFloristConfiguration();
        if (florists != null) {
            for (FloristProperties.FloristConfiguration fc : florists) {
                erosRetriever.getNewMessageList(fc.getFloristId(), fc.getSiteId());
            }
        }
    }
}
