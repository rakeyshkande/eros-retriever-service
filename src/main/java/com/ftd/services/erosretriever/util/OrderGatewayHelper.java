package com.ftd.services.erosretriever.util;

import com.ftd.eapi.message.service.v1.ErosMember;
import com.ftd.eapi.message.service.v1.OccasionCode;
import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.dto.ApplicableCharges;
import com.ftd.services.erosretriever.dto.BuyerDetails;
import com.ftd.services.erosretriever.dto.CodeList;
import com.ftd.services.erosretriever.dto.DeliveryGroups;
import com.ftd.services.erosretriever.dto.DiscountedCharges;
import com.ftd.services.erosretriever.dto.FeeCharges;
import com.ftd.services.erosretriever.dto.Fees;
import com.ftd.services.erosretriever.dto.LineItems;
import com.ftd.services.erosretriever.dto.NameValue;
import com.ftd.services.erosretriever.dto.OGSOrder;
import com.ftd.services.erosretriever.dto.PaymentDetails;
import com.ftd.services.erosretriever.dto.PaymentMethod;
import com.ftd.services.erosretriever.dto.PaymentMethodDetails;
import com.ftd.services.erosretriever.dto.PhoneNumbers;
import com.ftd.services.erosretriever.dto.RecipientDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Slf4j
@Component
public class OrderGatewayHelper {

    public OGSOrder buildOrder(OrderMessage order, ProcessedErosMessage message) {
        ErosMember erosMember = message.getSender();
        OGSOrder ogsOrder = new OGSOrder();
        ogsOrder.setOrderId(message.getNotification());
        ogsOrder.setLanguageId(GlobalConstants.OGS_LANGUAGE_ID);
        ogsOrder.setOrderChannel(GlobalConstants.WEBGIFTS_ORDER_CHANNEL);
        ogsOrder.setOrderType(GlobalConstants.OGS_ORDER_TYPE);
        ogsOrder.setOrderTimestamp(this.getFormattedDate(new Date()));

        CodeList codeList = new CodeList();
        List<String> stringList = new ArrayList<>();
        stringList.add(GlobalConstants.WEBGIFTS_SOURCE_CODE);
        codeList.setSource(stringList);
        codeList.setPromo(new ArrayList<>());
        ogsOrder.setCodeList(codeList);

        List<NameValue> fraud = new ArrayList<>();
        ogsOrder.setFraud(fraud);

        ogsOrder.setBuyerDetails(this.buildBuyerDetails(erosMember));
        ogsOrder.setPaymentDetails(this.buildPaymentDetails());

        List<NameValue> orderAmounts = new ArrayList<>();
        NameValue nv = new NameValue();
        nv.setName(GlobalConstants.OGS_ORDER_TOTAL);
        nv.setValue(String.valueOf(order.getPrice()));
        orderAmounts.add(nv);
        nv = new NameValue();
        nv.setName(GlobalConstants.OGS_DISCOUNT_AMOUNT);
        nv.setValue(GlobalConstants.ZERO_DOLLAR_AMOUNT);
        orderAmounts.add(nv);
        ogsOrder.setOrderAmounts(orderAmounts);

        ogsOrder.setDeliveryGroups(this.buildDeliveryGroups(order, ogsOrder));

        return ogsOrder;
    }

    public String getFormattedDate(Date submittedDate) {
        String currentDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(submittedDate);
        return currentDate + ".000Z";
    }

    private BuyerDetails buildBuyerDetails(ErosMember erosMember) {
        BuyerDetails buyer = new BuyerDetails();
        buyer.setFirstName(checkForMaxLength(erosMember.getMemberCode(), 20));
        buyer.setLastName(checkForMaxLength(erosMember.getBusinessName(), 20));
        buyer.setAddress1(checkForMaxLength(erosMember.getAddress(), 45));
        buyer.setCity(checkForMaxLength(erosMember.getCity(), 30));
        buyer.setState(erosMember.getStateCode());
        buyer.setPostalCode(checkForMaxLength(erosMember.getPostalCode(), 12));
        String floristState = erosMember.getMemberCode().substring(0, 2);
        try {
            int stateCode = Integer.parseInt(floristState);
            if (stateCode < 70 || stateCode == 90) {
                buyer.setCountryCode(GlobalConstants.OGS_COUNTRY_CODE_US);
            } else {
                buyer.setCountryCode(GlobalConstants.OGS_COUNTRY_CODE_CA);
            }
        } catch (Exception e) {
            log.error("Invalid state code: {}", erosMember.getMemberCode());
            buyer.setCountryCode(GlobalConstants.OGS_COUNTRY_CODE_US);
        }
        buyer.setEmailOptIn(false);
        List<PhoneNumbers> phones = new ArrayList<>();
        PhoneNumbers phone = new PhoneNumbers();
        phone.setPhoneNumber(erosMember.getPhone());
        phones.add(phone);
        buyer.setPhoneNumbers(phones);

        return buyer;
    }

    private PaymentDetails buildPaymentDetails() {
        PaymentDetails paymentDetails = new PaymentDetails();
        paymentDetails.setCurrencyCode(GlobalConstants.OGS_CURRENCY_CODE);
        List<PaymentMethod> paymentMethods = new ArrayList<>();
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setPaymentMethodType(GlobalConstants.OGS_PAYMENT_TYPE_NO_PAYMENT);
        PaymentMethodDetails paymentMethodDetails = new PaymentMethodDetails();
        List<NameValue> authorizationDetails = new ArrayList<>();
        paymentMethodDetails.setAuthorizationDetails(authorizationDetails);
        paymentMethod.setPaymentMethodDetails(paymentMethodDetails);
        paymentMethods.add(paymentMethod);
        paymentDetails.setPaymentMethod(paymentMethods);

        return paymentDetails;
    }

    private List<DeliveryGroups> buildDeliveryGroups(OrderMessage order, OGSOrder ogsOrder) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        List<DeliveryGroups> deliveryGroups = new ArrayList<>();
        DeliveryGroups deliveryGroup = new DeliveryGroups();
        deliveryGroup.setDeliveryGroupId(ogsOrder.getOrderId());
        deliveryGroup.setGiftMessage(order.getCardMessage());
        try {
            XMLGregorianCalendar xgc = order.getDeliveryDate();
            GregorianCalendar gc = xgc.toGregorianCalendar();
            deliveryGroup.setDeliveryDate(sdf.format(gc.getTime()));
        } catch (Exception e) {
            log.error("Invalid delivery date, using today");
            deliveryGroup.setDeliveryDate(sdf.format(new Date()));
        }
        OccasionCode oc = order.getOccasionCode();
        deliveryGroup.setOccasion(oc.value());

        deliveryGroup.setRecipientDetails(this.buildRecipientDetails(order));
        deliveryGroup.setLineItems(this.buildLineItems(order, ogsOrder));
        deliveryGroups.add(deliveryGroup);

        return deliveryGroups;
    }

    private RecipientDetails buildRecipientDetails(OrderMessage order) {
        RecipientDetails recip = new RecipientDetails();
        String[] parts = order.getRecipientName().split(" ");
        String firstName = parts[0];
        StringBuilder bld = new StringBuilder();
        for (int i = 1; i < parts.length; i++) {
            if (!bld.toString().equals("")) {
                bld.append(" ");
            }
            bld.append(parts[i]);
        }
        String lastName = bld.toString();
        if (lastName.isEmpty()) {
            lastName = GlobalConstants.OGS_DEFAULT_LAST_NAME;
        }
        recip.setFirstName(checkForMaxLength(firstName, 20));
        recip.setLastName(checkForMaxLength(lastName, 20));
        recip.setAddress1(checkForMaxLength(order.getRecipientStreetAddress(), 45));
        recip.setCity(checkForMaxLength(order.getRecipientCity(), 30));
        recip.setState(order.getRecipientStateCode());
        recip.setPostalCode(checkForMaxLength(order.getRecipientPostalCode(), 12));
        recip.setCountryCode(order.getRecipientCountryCode());
        if (recip.getCountryCode() == null) {
            recip.setCountryCode(GlobalConstants.OGS_COUNTRY_CODE_US);
        }
        recip.setAddressType(GlobalConstants.OGS_RECIPIENT_ADDRESS_TYPE);
        List<PhoneNumbers> phones = new ArrayList<>();
        PhoneNumbers phone = new PhoneNumbers();
        phone.setPhoneNumber(checkForMaxLength(order.getRecipientPhoneNumber(), 20));
        phones.add(phone);
        recip.setPhoneNumbers(phones);

        List<NameValue> avs = new ArrayList<>();
        NameValue nv = new NameValue();
        nv.setName(GlobalConstants.OGS_AVS_PERFORMED);
        nv.setValue("false");
        avs.add(nv);
        recip.setAddressVerification(avs);

        return recip;
    }

    private List<LineItems> buildLineItems(OrderMessage order, OGSOrder ogsOrder) {
        List<LineItems> lineItems = new ArrayList<>();
        LineItems item = new LineItems();
        item.setLineItemId(ogsOrder.getOrderId());
        item.setProductId(order.getFirstChoiceProductCode());
        item.setWebsiteId(order.getFirstChoiceProductCode());
        item.setFulfillmentChannel(GlobalConstants.WEBGIFTS_FULFILLMENT_CHANNEL);
        item.setServiceLevel(GlobalConstants.WEBGIFTS_SERVICE_LEVEL);
        item.setProductType(GlobalConstants.WEBGIFTS_PRODUCT_TYPE);
        item.setDeliveryType(GlobalConstants.WEBGIFTS_DELIVERY_TYPE);

        CodeList codeList = new CodeList();
        List<String> stringList = new ArrayList<>();
        stringList.add(GlobalConstants.WEBGIFTS_SOURCE_CODE);
        codeList.setSource(stringList);
        codeList.setPromo(new ArrayList<>());
        item.setCodeList(codeList);

        List<NameValue> lineItemAmounts = new ArrayList<>();
        NameValue nv = new NameValue();
        nv.setName(GlobalConstants.OGS_RETAIL_PRODUCT_AMOUNT);
        nv.setValue(String.valueOf(order.getPrice()));
        lineItemAmounts.add(nv);
        nv = new NameValue();
        nv.setName(GlobalConstants.OGS_SALE_PRODUCT_AMOUNT);
        nv.setValue(String.valueOf(order.getPrice()));
        lineItemAmounts.add(nv);
        nv = new NameValue();
        nv.setName(GlobalConstants.OGS_DISCOUNT_AMOUNT);
        nv.setValue(GlobalConstants.ZERO_DOLLAR_AMOUNT);
        lineItemAmounts.add(nv);
        nv = new NameValue();
        nv.setName(GlobalConstants.OGS_TAX_AMOUNT);
        nv.setValue(GlobalConstants.ZERO_DOLLAR_AMOUNT);
        lineItemAmounts.add(nv);
        nv = new NameValue();
        nv.setName(GlobalConstants.OGS_ITEM_TOTAL);
        nv.setValue(String.valueOf(order.getPrice()));
        lineItemAmounts.add(nv);
        item.setLineItemAmounts(lineItemAmounts);

        Fees fees = new Fees();
        ApplicableCharges applicableCharges = new ApplicableCharges();
        DiscountedCharges discountedCharges = new DiscountedCharges();
        List<FeeCharges> surcharges = new ArrayList<>();
        List<FeeCharges> deliveryCharges = new ArrayList<>();
        applicableCharges.setSurCharges(surcharges);
        applicableCharges.setDeliveryCharges(deliveryCharges);
        applicableCharges.setTotalApplicableCharges(new BigDecimal(GlobalConstants.ZERO_DOLLAR_AMOUNT));
        discountedCharges.setSurCharges(surcharges);
        discountedCharges.setDeliveryCharges(deliveryCharges);
        discountedCharges.setTotalDiscountedCharges(new BigDecimal(GlobalConstants.ZERO_DOLLAR_AMOUNT));
        fees.setApplicableCharges(applicableCharges);
        fees.setDiscountedCharges(discountedCharges);
        item.setFees(fees);

        lineItems.add(item);
        return lineItems;
    }

    private String checkForMaxLength(String inputValue, int length) {
        String returnValue = null;
        if (inputValue != null) {
            if (inputValue.length() <= length) {
                returnValue = inputValue;
            } else {
                returnValue = inputValue.substring(0, length);
            }
        }
        return returnValue;
    }
}
