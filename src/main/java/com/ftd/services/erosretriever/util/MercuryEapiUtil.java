package com.ftd.services.erosretriever.util;

import com.ftd.eapi.message.service.v1.ConfirmMessageByReference;
import com.ftd.eapi.message.service.v1.ConfirmMessageResponse;
import com.ftd.eapi.message.service.v1.ConfirmResponse;
import com.ftd.eapi.message.service.v1.ObjectFactory;
import com.ftd.eapi.message.service.v1.SimpleResponse;
import com.ftd.services.erosretriever.config.GlobalConstants;
import com.ftd.services.erosretriever.domain.event.FloristInboundEvent;
import com.ftd.services.erosretriever.domain.event.SiteId;
import com.ftd.services.erosretriever.event.publisher.internal.FloristGatewayInboundPublisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBElement;
import java.util.List;

@Slf4j
@Component
public class MercuryEapiUtil {

    @Autowired
    private ErosSoapClient erosSoapClient;

    @Autowired
    private FloristGatewayInboundPublisher floristGatewayInboundPublisher;

    public boolean confirmMessage(String referenceNumber, String floristId) {
        boolean result = false;

        ObjectFactory factory = new ObjectFactory();
        ConfirmMessageByReference conf = new ConfirmMessageByReference();
        conf.setMainMemberCode(floristId);
        conf.getReferenceNumber().add(referenceNumber);
        JAXBElement<ConfirmMessageByReference> confJAXB = factory.createConfirmMessageByReference(conf);
        JAXBElement<ConfirmMessageResponse> confResponse = (JAXBElement<ConfirmMessageResponse>) erosSoapClient.call(confJAXB, floristId);
        ConfirmMessageResponse cmResponse = confResponse.getValue();
        ConfirmResponse crResponse = cmResponse.getConfirmResponse();
        List<SimpleResponse> sResponseList = crResponse.getSimpleResponse();
        for (SimpleResponse sResponse : sResponseList) {
            log.info(sResponse.getCode() + " " + sResponse.getKey() + " " + sResponse.getMessage());
            if (sResponse.getCode() != null && sResponse.getCode().equalsIgnoreCase(GlobalConstants.VERIFIED)) {
                result = true;
            }
        }

        return result;
    }

    public boolean enqueueMessageId(String floristId, Long messageId, String siteId) {
        boolean response = true;

        FloristInboundEvent floristInboundEvent = FloristInboundEvent.builder()
                .floristId(floristId)
                .messageId(messageId)
                .siteId(SiteId.valueOf(siteId))
                .build();
        try {
            floristGatewayInboundPublisher.publish(floristInboundEvent);
            log.info("message sent to pubsub");
        } catch (Exception e) {
            log.error("Error occurred while creating the event ", e);
            response = false;
        }
        return response;
    }

}
