package com.ftd.services.erosretriever.util;

import com.ftd.eapi.message.service.v1.OrderRelatedMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.domain.Data;
import com.ftd.services.erosretriever.domain.ErosMember;
import com.ftd.services.erosretriever.domain.Recipient;

import java.util.Optional;

import static com.ftd.services.erosretriever.util.DateUtils.toDate;

public class TransformerUtil {

    public static ErosMember populateMember(com.ftd.eapi.message.service.v1.ErosMember erosMember) {
        ErosMember erosMemberForOrder = new ErosMember();

        erosMemberForOrder.setAddress(erosMember.getAddress());
        erosMemberForOrder.setBusinessName(erosMember.getBusinessName());
        erosMemberForOrder.setCity(erosMember.getCity());
        erosMemberForOrder.setListingCode(erosMember.getListingCode());
        erosMemberForOrder.setMemberClassification(erosMember.getMemberClassification());
        erosMemberForOrder.setMemberCode(erosMember.getMemberCode());
        erosMemberForOrder.setPhone(erosMember.getPhone());
        erosMemberForOrder.setPostalCode(erosMember.getPostalCode());
        erosMemberForOrder.setStateCode(erosMember.getStateCode());

        return erosMemberForOrder;
    }

    public static Recipient populateRecipient(ProcessedErosMessage message) {
        OrderRelatedMessage orderRelatedMessage = (OrderRelatedMessage) message.getErosMessage();

        Recipient recipient = new Recipient();
        recipient.setAddress1(orderRelatedMessage.getRecipientAddress());
        recipient.setCity(orderRelatedMessage.getRecipientCity());
        recipient.setCityStateZip(orderRelatedMessage.getRecipientCityStateZip());
        recipient.setCountryCode(orderRelatedMessage.getRecipientCountryCode());
        recipient.setName(orderRelatedMessage.getRecipientName());
        recipient.setPhoneNumber(orderRelatedMessage.getRecipientPhone());
        recipient.setZip(orderRelatedMessage.getRecipientPostalCode());
        recipient.setState(orderRelatedMessage.getRecipientStateCode());
        return recipient;
    }

    public static Data populateOrderRelatedMessageInfo(ProcessedErosMessage processedErosMessage, OrderRelatedMessage orderRelatedMessage) {
        Data data = Data.builder()
                .deliveryDateString(orderRelatedMessage.getDeliveryDateString())
                .referenceNumber(processedErosMessage.getReferenceNumber())
                .adminSequenceNumber(processedErosMessage.getAdminSequenceNumber())
                .externalReferenceNumber(orderRelatedMessage.getExternalReferenceNumber())
                .operator(orderRelatedMessage.getOperator())
                .correlationReference(orderRelatedMessage.getCorrelationReference())
                .receivingMemberCode(orderRelatedMessage.getReceivingMemberCode())
                .recipient(populateRecipient(processedErosMessage))
                .relatedOrderReferenceNumber(orderRelatedMessage.getRelatedOrderReferenceNumber())
                .relatedOrderSequenceNumber(orderRelatedMessage.getRelatedOrderSequenceNumber())
                .orderDateString(orderRelatedMessage.getOrderDateString())
                .sendingMemberCode(orderRelatedMessage.getSendingMemberCode())
                .service(orderRelatedMessage.getService().value())
                .reasonText(orderRelatedMessage.getReasonText())
                .adminSequenceNumber(processedErosMessage.getAdminSequenceNumber())
                .error(processedErosMessage.getError())
                .messageId(processedErosMessage.getMessageId())
                .messageStatus(processedErosMessage.getMessageStatus())
                .messageType(processedErosMessage.getMessageType())
                .relatedMessageId(processedErosMessage.getRelatedMessageId())
                .orderSequenceNumber(processedErosMessage.getOrderSequenceNumber())
                .transmissionId(processedErosMessage.getTransmissionId())
                .transmissionStatus(processedErosMessage.getTransmissionStatus())
                .warning(processedErosMessage.getWarning())
                .build();

        if (Optional.ofNullable(orderRelatedMessage.getDeliveryDate()).isPresent()) {
            data.setDeliveryDate(toDate(orderRelatedMessage.getDeliveryDate()));
        }
        if (Optional.ofNullable(orderRelatedMessage.getOrderDate()).isPresent()) {
            data.setOrderDate(toDate(orderRelatedMessage.getOrderDate()));
        }

        checkNotNullAndSetData(processedErosMessage, data);

        return data;
    }

    public static void checkNotNullAndSetData(ProcessedErosMessage processedErosMessage, Data data) {
        if (Optional.ofNullable(processedErosMessage.getNewFiller()).isPresent()) {
            data.setNewFiller(populateMember(processedErosMessage.getNewFiller()));
        }
        if (Optional.ofNullable(processedErosMessage.getMessageReceivedDate()).isPresent()) {
            data.setMessageReceivedDate(toDate(processedErosMessage.getMessageReceivedDate()));
        }
        if (Optional.ofNullable(processedErosMessage.getReceiver()).isPresent()) {
            data.setFulfillingFlorist(populateMember(processedErosMessage.getReceiver()));
        }
        if (Optional.ofNullable(processedErosMessage.getSender()).isPresent()) {
            data.setSendingFlorist(populateMember(processedErosMessage.getSender()));
        }
        if (Optional.ofNullable(processedErosMessage.getTransmissionDate()).isPresent()) {
            data.setTransmissionDate(toDate(processedErosMessage.getTransmissionDate()));
        }
    }
}
