package com.ftd.services.erosretriever.domain;

import com.ftd.services.erosretriever.types.EventType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FloristMessageNotification {
    private String fulfillmentChannel;
    private Data data;
    private EventType eventName;
    private String source;
}
