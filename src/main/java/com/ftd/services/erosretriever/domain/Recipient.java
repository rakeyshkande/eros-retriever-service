package com.ftd.services.erosretriever.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Recipient {

    private String phoneNumber;

    private String name;

    private String recipientCityStateZip;

    private String cityStateZip;

    private String address1;

    private String state;

    private String zip;

    private String countryCode;

    private String city;
}
