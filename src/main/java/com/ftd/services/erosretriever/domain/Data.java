package com.ftd.services.erosretriever.domain;

import com.ftd.eapi.message.service.v1.CodeMessagePair;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@lombok.Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Data {
    private String reasonText;
    private Integer relatedOrderSequenceNumber;
    private Long messageId;
    private Date messageReceivedDate;
    private Date transmissionDate;
    private String operator;
    private String messageStatus;
    private String correlationReference;
    private Long transmissionId;
    private String receivingMemberCode;
    private Integer adminSequenceNumber;
    private String transmissionStatus;
    private String messageType;
    private Long relatedMessageId;
    private String sendingMemberCode;
    private String relatedOrderReferenceNumber;
    private String referenceNumber;
    @Pattern(regexp = "FTD|TEL", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String service;
    private Recipient recipient;
    private String externalReferenceNumber;
    private Date deliveryDate;
    private Date orderDate;
    private String messageText;
    private ErosMember fulfillingFlorist;
    private Integer orderSequenceNumber;
    private ErosMember newFiller;
    private String orderDateString;
    private ErosMember sendingFlorist;
    private String priority;
    private List<CodeMessagePair> error;
    private Boolean slimcast;
    private String deliveryDateString;
    private List<CodeMessagePair> warning;
    private BigDecimal price;
    private String newFillerMemberCode;
    private String newFillerListingCode;
    private String requestCode;
}
