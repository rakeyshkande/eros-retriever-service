package com.ftd.services.erosretriever.domain.event;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FloristInboundEvent {

    private String floristId;
    private long messageId;
    private SiteId siteId;

}
