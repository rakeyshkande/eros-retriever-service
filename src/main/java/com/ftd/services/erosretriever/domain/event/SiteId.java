package com.ftd.services.erosretriever.domain.event;

public enum SiteId {

    ftd, proflowers
}
