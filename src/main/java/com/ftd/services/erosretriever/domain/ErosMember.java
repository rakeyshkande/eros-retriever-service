package com.ftd.services.erosretriever.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErosMember {
    private String address;
    private String businessName;
    private String city;
    private String listingCode;
    private String memberClassification;
    private String memberCode;
    private String phone;
    private String postalCode;
    private String stateCode;
}
