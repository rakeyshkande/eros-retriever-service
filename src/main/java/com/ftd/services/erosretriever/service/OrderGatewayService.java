package com.ftd.services.erosretriever.service;

import com.ftd.eapi.message.service.v1.OrderMessage;
import com.ftd.eapi.message.service.v1.ProcessedErosMessage;
import com.ftd.services.erosretriever.api.request.OGSRequest;
import com.ftd.services.erosretriever.event.publisher.OrderGatewayOutboundPublisher;
import com.ftd.services.erosretriever.util.OrderGatewayHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class OrderGatewayService {

    @Autowired
    private OrderGatewayHelper orderGatewayHelper;

    @Autowired
    private OrderGatewayOutboundPublisher orderGatewayOutboundPublisher;

    public void processWebgiftOrder(OrderMessage order, ProcessedErosMessage message, String siteId) {
        OGSRequest ogsRequest = new OGSRequest();
        ogsRequest.setOrder(orderGatewayHelper.buildOrder(order, message));

        log.info("Posting FTD Web gift order {} into OG topic", ogsRequest.getOrder().toString());
        orderGatewayOutboundPublisher.publish(ogsRequest);
    }
}
