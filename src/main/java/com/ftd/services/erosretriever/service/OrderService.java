package com.ftd.services.erosretriever.service;

import com.ftd.services.erosretriever.domain.FloristMessageNotification;
import com.ftd.services.erosretriever.event.publisher.OrderNotificationOutboundPublisher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class OrderService {

    @Autowired
    private OrderNotificationOutboundPublisher orderNotificationOutboundPublisher;

    public void orderNotification(FloristMessageNotification floristMessageNotification) {
        log.info("Posting Order Notification {} into Order topic", floristMessageNotification.toString());
        orderNotificationOutboundPublisher.publish(floristMessageNotification);
    }
}
